#version 400

in vec4 fragcolor;
out vec4 color;

void main()
{
    color = fragcolor;
}
