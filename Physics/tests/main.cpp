#include "gtest/gtest.h"
#include <simulation/scene.h>
#include "simulationentitymock.h"

TEST (Scene, editing_entity_list) {
    Scene scene;
    MockSimulationEntity mock0(0);
    MockSimulationEntity mock1(1);
    MockSimulationEntity mock2(2);

    EXPECT_EQ (0, scene.addEntity(&mock0));
    EXPECT_EQ (1, scene.addEntity(&mock1));
    EXPECT_EQ (2, scene.addEntity(&mock2));

    EXPECT_EQ(2, ((MockSimulationEntity*)scene.getEntity(2))->id);

    EXPECT_EQ(true, scene.removeEntity(1));
    EXPECT_EQ(false, scene.removeEntity(1));

    EXPECT_EQ (1, scene.addEntity(&mock1));
}


int main(int argc, char *argv[]){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
