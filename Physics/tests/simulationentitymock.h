#pragma once

#include "gtest/gtest.h"

#include <collision/collisionresolver.h>
#include <simulation/simulationentity.h>
#include <physics/physicscomponent.h>
#include <glm/glm.hpp>
#include "gmock/gmock.h"

using glm::vec3;
class ComplexShape;

class MockSimulationEntity : public SimulationEntity {
public:
    int id;
    MockSimulationEntity(int id):id(id){}
    MOCK_METHOD0(getPhysicsComponent, PhysicsComponent*());
    MOCK_METHOD0(getPosition, vec3());
    MOCK_METHOD0(getRotation, vec3());
    MOCK_METHOD0(getComplexShape, ComplexShape*());
};

class MockPhysicsComponent : public PhysicsComponent {
public:
    MOCK_METHOD0(getVelocity, vec3());
    MOCK_METHOD3(setVelocity, void(float, float, float));
    MOCK_METHOD2(applyImpulse, void(vec3, vec3));
};

