#pragma once

#include "gtest/gtest.h"

#include <collision/shape.h>
#include <collision/complexshape.h>
#include <glm/glm.hpp>
#include "gmock/gmock.h"

using glm::vec3;


class MockShape : public Shape {
public:
    MOCK_METHOD0(getOffset, vec3());
    MOCK_METHOD0(getVolume, float());
    MOCK_METHOD0(getRotation, vec3());
};

class MockComplexShape : public ComplexShape {
public:
    MOCK_METHOD0(getPosition, vec3());
    MOCK_METHOD0(getVolume, float());
    MOCK_METHOD0(getInertiaTensor, mat3());
};
