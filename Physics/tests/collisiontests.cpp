#include "simulationentitymock.h"
#include "complexshapemock.h"

using ::testing::Return;
using ::testing::_;
using ::testing::Expectation;
using ::testing::FloatNear;
using ::testing::SaveArg;
using ::testing::DoAll;

using glm::mat3;

TEST (CollisionResolver, sphere_sphere_perpendicular_collision) {
    MockSimulationEntity mockSim1(1);
    MockShape shape1;
    MockComplexShape complex1;
    MockPhysicsComponent mockPhys1;

    mat3 inertiaTensor = mat3(0.4f); // 2/5*m*r^2
    vec3 impulse1,impulse2,angularImpulse1,angularImpulse2;

    EXPECT_CALL(mockSim1, getComplexShape())
            .Times(2)
            .WillRepeatedly(Return(&complex1));
    EXPECT_CALL(shape1, getVolume())
            .Times(2)
            .WillOnce(Return(1))
            .WillOnce(Return(1));
    EXPECT_CALL(mockSim1, getPhysicsComponent())
            .Times(2)
            .WillRepeatedly(Return(&mockPhys1));
    EXPECT_CALL(complex1, getInertiaTensor())
            .Times(2)
            .WillRepeatedly(Return(inertiaTensor));
    EXPECT_CALL(mockPhys1, applyImpulse(_,_))
            .Times(2)
            .WillOnce(DoAll(SaveArg<0>(&impulse1), SaveArg<1>(&angularImpulse1)))
            .WillOnce(DoAll(SaveArg<0>(&impulse2), SaveArg<1>(&angularImpulse2)));

    CollisionResolver::resolveMovableMovable(vec3(-1,0,0),
                                           vec3(2,0,0),
                                           &mockSim1,
                                           vec3(0,0,0),
                                           &mockSim1,
                                           vec3(0,0,0));

    EXPECT_FLOAT_EQ(impulse1.x, -2.f);
    EXPECT_FLOAT_EQ(impulse1.y, 0.f);
    EXPECT_FLOAT_EQ(impulse1.z, 0.f);
}
TEST (CollisionResolver, sphere_sphere_oblique_collision) {
    MockSimulationEntity mockSim1(1);
    MockShape shape1;
    MockComplexShape complex1;
    MockPhysicsComponent mockPhys1;

    mat3 inertiaTensor = mat3(4.f);
    inertiaTensor[2].z = 8.f;
    vec3 impulse1,impulse2,angularImpulse1,angularImpulse2;

    EXPECT_CALL(mockSim1, getComplexShape())
            .Times(2)
            .WillRepeatedly(Return(&complex1));
    EXPECT_CALL(shape1, getVolume())
            .Times(2)
            .WillOnce(Return(1))
            .WillOnce(Return(4));
    EXPECT_CALL(mockSim1, getPhysicsComponent())
            .Times(2)
            .WillRepeatedly(Return(&mockPhys1));
    EXPECT_CALL(complex1, getInertiaTensor())
            .Times(2)
            .WillRepeatedly(Return(inertiaTensor));
    EXPECT_CALL(mockPhys1, applyImpulse(_,_))
            .Times(2)
            .WillOnce(DoAll(SaveArg<0>(&impulse1), SaveArg<1>(&angularImpulse1)))
            .WillOnce(DoAll(SaveArg<0>(&impulse2), SaveArg<1>(&angularImpulse2)));

    CollisionResolver::resolveMovableMovable(vec3(-1,0,0),
                                           vec3(1,0,0),
                                           &mockSim1,
                                           vec3(0,0,0),
                                           &mockSim1,
                                           vec3(-1,1,0));

    EXPECT_FLOAT_EQ(impulse1.x, -2.f/1.375f);
    EXPECT_FLOAT_EQ(impulse1.y, 0.0f);
    EXPECT_FLOAT_EQ(impulse1.z, 0.f);
}


/*
TEST (CollisionResolver, sphere_sphere_perpendicular_collision) {
    MockSimulationEntity mockSim1(1);
    MockShape shape1;
    MockPhysicsComponent mockPhys1;
    EXPECT_CALL(mockSim1, getPhysicsComponent())
            .Times(4)
            .WillRepeatedly(Return(&mockPhys1));
    EXPECT_CALL(mockSim1, getPosition())
            .Times(2)
            .WillOnce(Return(vec3(0,0,0)))
            .WillOnce(Return(vec3(1,0,0)));
    EXPECT_CALL(mockPhys1, getVelocity())
            .Times(2)
            .WillOnce(Return(vec3(1,0,0)))
            .WillOnce(Return(vec3(0,0,0)));
    EXPECT_CALL(shape1, getOffset())
            .Times(2)
            .WillRepeatedly(Return(vec3(1,0,0)));
    EXPECT_CALL(mockPhys1, setVelocity(0,0,0));
    EXPECT_CALL(mockPhys1, setVelocity(1,0,0));

    CollisionResolver::resolveSphereSphere(&mockSim1, &shape1, &mockSim1, &shape1);
}

TEST (CollisionResolver, sphere_sphere_second_sphere_in_move) {
    MockSimulationEntity mockSim1(1);
    MockShape shape1;
    MockPhysicsComponent mockPhys1;
    EXPECT_CALL(mockSim1, getPhysicsComponent())
            .Times(4)
            .WillRepeatedly(Return(&mockPhys1));
    EXPECT_CALL(mockSim1, getPosition())
            .Times(2)
            .WillOnce(Return(vec3(0,0,0)))
            .WillOnce(Return(vec3(1,0,0)));
    EXPECT_CALL(mockPhys1, getVelocity())
            .Times(2)
            .WillOnce(Return(vec3(1,0,0)))
            .WillOnce(Return(vec3(0.5,0,0)));
    EXPECT_CALL(shape1, getOffset())
            .Times(2)
            .WillRepeatedly(Return(vec3(1,0,0)));
    Expectation set1 = EXPECT_CALL(mockPhys1, setVelocity(_,_,_))
            .With(std::tuple<float, float, float>(0.5f,0.f,0.f));
    EXPECT_CALL(mockPhys1, setVelocity(_,_,_))
            .With(std::tuple<float, float, float>(1.f,0.f,0.f))
            .After(set1);

    CollisionResolver::resolveSphereSphere(&mockSim1, &shape1, &mockSim1, &shape1);
}


TEST (CollisionResolver, sphere_sphere_second_oblique) {
    MockSimulationEntity mockSim1(1);
    MockShape shape1;
    MockPhysicsComponent mockPhys1;
    EXPECT_CALL(mockSim1, getPhysicsComponent())
            .Times(4)
            .WillRepeatedly(Return(&mockPhys1));
    EXPECT_CALL(mockSim1, getPosition())
            .Times(2)
            .WillOnce(Return(vec3(0,0,0)))
            .WillOnce(Return(vec3(1,0,0)));
    EXPECT_CALL(mockPhys1, getVelocity())
            .Times(2)
            .WillOnce(Return(vec3(1,0.3,-0.4)))
            .WillOnce(Return(vec3(0.5,-0.2,0.1)));
    EXPECT_CALL(shape1, getOffset())
            .Times(2)
            .WillRepeatedly(Return(vec3(1,0,0)));
    Expectation set1 = EXPECT_CALL(mockPhys1, setVelocity(_,_,_))
            .With(std::tuple<float, float, float>(0.5f,0.3f,-0.4f));
    EXPECT_CALL(mockPhys1, setVelocity(_,_,_))
            .With(std::tuple<float, float, float>(1.f,-0.2f,0.1f))
            .After(set1);

    CollisionResolver::resolveSphereSphere(&mockSim1, &shape1, &mockSim1, &shape1);
}


TEST (CollisionResolver, plane_sphere_perpendicular) {
    MockSimulationEntity mockSim1(1);
    MockShape shape1;
    MockPhysicsComponent mockPhys1;
    EXPECT_CALL(mockSim1, getPhysicsComponent())
            .WillRepeatedly(Return(&mockPhys1));
    EXPECT_CALL(mockSim1, getRotation())
            .Times(2)
            .WillRepeatedly(Return(vec3(0, 90, 0)));
    EXPECT_CALL(shape1, getRotation())
            .WillRepeatedly(Return(vec3(0, 0, 0)));
    EXPECT_CALL(mockPhys1, getVelocity())
            .Times(1)
            .WillOnce(Return(vec3(1,0,0)));
    EXPECT_CALL(mockPhys1, setVelocity(FloatNear(-1.f,0.001f),FloatNear(0.f,0.00001f),FloatNear(0.f,0.00001f)));

    CollisionResolver::resolvePlaneSphere(&mockSim1, &shape1, &mockSim1, &shape1);
}


TEST (CollisionResolver, plane_sphere_oblique) {
    MockSimulationEntity mockSim1(1);
    MockShape shape1;
    MockPhysicsComponent mockPhys1;
    EXPECT_CALL(mockSim1, getPhysicsComponent())
            .WillRepeatedly(Return(&mockPhys1));
    EXPECT_CALL(mockSim1, getRotation())
            .Times(2)
            .WillRepeatedly(Return(vec3(0, 90, 0)));
    EXPECT_CALL(shape1, getRotation())
            .WillRepeatedly(Return(vec3(0, 0, 0)));
    EXPECT_CALL(mockPhys1, getVelocity())
            .Times(1)
            .WillOnce(Return(vec3(1,0.3,-0.2)));
    EXPECT_CALL(mockPhys1, setVelocity(FloatNear(-1,0.001),FloatNear(0.3,0.00001),FloatNear(-0.2,0.00001)));

    CollisionResolver::resolvePlaneSphere(&mockSim1, &shape1, &mockSim1, &shape1);
}*/
