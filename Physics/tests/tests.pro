include(../defaults.pri)

TEMPLATE  = app
CONFIG   += console
CONFIG   -= app_bundle
SOURCES  += main.cpp \
    collisiontests.cpp \
    complexshapetests.cpp

LIBS += -L$$PWD/../libs/gtest/lib/ -lgtestd
LIBS += -L$$PWD/../libs/gtest/lib/ -lgmock
LIBS += -L../src/debug -lPhysics

INCLUDEPATH += $$PWD/../libs/gtest/include
DEPENDPATH += $$PWD/../libs/gtest/include
INCLUDEPATH += $$PWD/../src
DEPENDPATH += $$PWD/../src

HEADERS += \
    simulationentitymock.h \
    complexshapemock.h
