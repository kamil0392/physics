#include "complexshapemock.h"
#include <collision/complexshape.h>
#include "glm/glm.hpp"

using glm::vec3;

using ::testing::Return;
using ::testing::_;
using ::testing::Expectation;
using ::testing::FloatNear;

TEST (RecalcVolume, test1) {
    MockShape shape1;
    EXPECT_CALL(shape1, getVolume())
            .Times(6)
            .WillOnce(Return(12.f))
            .WillOnce(Return(13.f))
            .WillOnce(Return(12.f))
            .WillOnce(Return(13.f))
            .WillOnce(Return(12.f))
            .WillOnce(Return(13.f));
    EXPECT_CALL(shape1, getOffset())
            .WillRepeatedly(Return(vec3(0.f)));
    list<Shape*> shapeList = {&shape1, &shape1};
    ComplexShape *complexShape = new ComplexShape(shapeList, vec3(0.f), vec3(0.f));

    EXPECT_EQ (25, complexShape->getVolume());
}

TEST (RecalcVolume, test2) {
    MockShape shape1;
    EXPECT_CALL(shape1, getVolume())
            .Times(6)
            .WillRepeatedly(Return(10.f));
    EXPECT_CALL(shape1, getOffset())
            .Times(2)
            .WillOnce(Return(vec3(0.f)))
            .WillOnce(Return(vec3(1.f)));
    list<Shape*> shapeList = {&shape1, &shape1};
    ComplexShape *complexShape = new ComplexShape(shapeList, vec3(0.f), vec3(0.f));

    EXPECT_EQ (20, complexShape->getVolume());
    vec3 pos = complexShape->getPosition();
    EXPECT_EQ (0.5f, pos.x);
    EXPECT_EQ (0.5f, pos.y);
    EXPECT_EQ (0.5f, pos.z);
}
