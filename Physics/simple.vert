#version 400

layout(location = 0) in vec3 position;

out vec3 fragcolor;

void main()
{
    gl_Position = vec4(position, 1.0);
    fragcolor = vec3(0.3,0.5,0.7);
}
