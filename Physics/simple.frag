#version 400

in vec3 fragcolor;
out vec3 color;

void main()
{
    color = fragcolor;
}
