#version 400

uniform vec4 entity_color;

in vec4 fragNormal;
in vec3 fragPosition;
out vec4 color;

void main()
{
    vec3 lightDir   = vec3(-1, -0.8, 3.0);
    vec3 lightColor = vec3(1.0, 1.0, 1.0);
    vec3 fragNormal_norm = normalize(fragNormal.xyz);

    // ambient
    float ambient_strength = 0.3;
    vec3 ambient = ambient_strength*entity_color.xyz;

    // diffuse
    float diffuse_strength = 0.75;
    vec3 diffuse = diffuse_strength*((1 + dot(fragNormal_norm, lightDir))/2)*entity_color.xyz;

    color = vec4(ambient + diffuse, 0.4);
}
