#version 400

layout(location = 0) in vec3 position;

uniform mat4 mvp_matrix;
uniform vec4 entity_color;

out vec4 fragcolor;

void main()
{
    gl_Position = vec4(position, 1.0) * mvp_matrix;

    fragcolor = entity_color;
}
