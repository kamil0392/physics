TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS = src \
	app \
	tests
	
app.depends = src
tests.depends = src

DISTFILES += \
    simple.vert \
    simple.frag \
    simple_mvp.vert \
    simple_mvp.frag \
    simple_shading_mvp.vert \
    simple_shading_mvp.frag \
    uniform_normal_mvp.vert \
    uniform_normal_mvp.frag

OTHER_FILES += \
    defaults.pri

