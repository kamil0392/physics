#pragma once

#include "glm/glm.hpp"
#include "simulation/camera.h"
#include "render/matrixprovider.h"
#include "render/renderprovider.h"

using glm::mat4;

class Context
{
public:
    Context();

    void setDelay(float delay);
    void setCamera(Camera *camera);
    void setMatrixProvider(MatrixProvider *matrixProvider);
    void setRenderProvider(RenderProvider *renderProvider);

    mat4 getCameraMatrix();
    float getDelay();
    mat4 getPerspectiveMatrix();
    RenderProvider *getRenderProvider();

protected:
    float delay;
    Camera *camera;
    MatrixProvider *matrixProvider;
    RenderProvider *renderProvider;
};

