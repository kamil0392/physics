#include "simulation/simulationentity.h"
#include "simulation/behaviour/behaviour.h"

SimulationEntity::~SimulationEntity(){
    delete complexShape;
    delete physicsComponent;
    for(auto behaviour : behaviours){
        delete behaviour;
    }
}

SimulationEntity::SimulationEntity(Scene *scene, ShaderId shaderId, ComplexShape *complexShape):
    shaderId(shaderId), complexShape(complexShape), scene(scene)
{
    physicsComponent = nullptr;
}

SimulationEntity::SimulationEntity(Scene *scene, ShaderId shaderId, ComplexShape *complexShape, PhysicsComponent *physicsComponent):
    shaderId(shaderId), complexShape(complexShape), scene(scene), physicsComponent(physicsComponent)
{
    physicsComponent->setSimulationEntity(this);
}

void SimulationEntity::update(Context *context){
    if (physicsComponent != nullptr){
        physicsComponent->update(context);
    }
    for (auto behaviour : behaviours){
        behaviour->update();
    }
}

void SimulationEntity::render(Context *context){
    context->getRenderProvider()->render(shaderId, context, complexShape);
}

void SimulationEntity::move(glm::vec3 displacementVec){
    complexShape->move(displacementVec);
}

void SimulationEntity::rotate(glm::mat4 rotationMatrix){
    complexShape->rotate(rotationMatrix);
}

void SimulationEntity::addBehaviour(Behaviour *behaviour){
    behaviour->setSimulationEntity(this);
    behaviours.push_back(behaviour);
}
