#pragma once

#include "global.h"
#include "shaders/ShaderProvider.h"
#include "camera.h"
#include "simulationentity.h"
#include "primitiveid.h"
#include "simulation/context.h"
#include <map>
#include <string>
#include "collision/broadphaser.h"

class SimulationEntity;

class TEST_COMMON_DLLSPEC Scene
{
public:
    Scene(){}

    void init();
    void update(Context *context);
    void render(Context *context);

    Camera* getCamera(){ return &camera; }

    int addEntity(SimulationEntity *entity);
    SimulationEntity *getEntity(int id);
    bool removeEntity(int id);

protected:
    void createScene(int scenarioNumber);
    map<int, SimulationEntity*> entityMap;
    Camera camera;
    BroadPhaser broadPhaser;
};
