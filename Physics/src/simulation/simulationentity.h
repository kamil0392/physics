#pragma once

#include "global.h"
#include "render/rendercomponent.h"
#include "render/matrixprovider.h"
#include "glm/glm.hpp"
#include "collision/ComplexShape.h"
#include "simulation/scene.h"
#include "simulation/context.h"
#include "physics/physicscomponent.h"

using std::list;

class PhysicsComponent;
class Scene;
class Behaviour;

class TEST_COMMON_DLLSPEC SimulationEntity
{
public:
    SimulationEntity(){}
    ~SimulationEntity();
    SimulationEntity(Scene *scene, ShaderId shaderId, ComplexShape *complexShape);
    SimulationEntity(Scene *scene, ShaderId shaderId, ComplexShape *complexShape, PhysicsComponent *physicsComponent);

    void render(Context *context);
    virtual void update(Context *context);
    void move(glm::vec3 displacementVec);
    void rotate(glm::mat4 rotationMatrix);

    bool isCollidable() {return collidable; }
    bool isMovable() {return physicsComponent != nullptr;}

    virtual glm::vec3 getPosition() { return complexShape->getPosition(); }
    virtual glm::vec3 getRotation() { return complexShape->getRotation(); }
    virtual glm::mat4 getRotationMatrix() { return complexShape->getRotationMatrix(); }
    virtual ComplexShape* getComplexShape() { return complexShape; }
    Scene* getScene() { return scene; }

    virtual PhysicsComponent *getPhysicsComponent() { return physicsComponent; }

    void setCollidability(bool collidable) { this->collidable = collidable; }

    void addBehaviour(Behaviour *behaviour);
    bool hasBehaviours() { return !behaviours.empty(); }
    list<Behaviour*> *getBehaviours() { return &behaviours; }

protected:
    ShaderId shaderId;
    PhysicsComponent* physicsComponent = nullptr;
    ComplexShape* complexShape;
    Scene* scene;

    bool collidable = true;
    list<Behaviour*> behaviours;
};
