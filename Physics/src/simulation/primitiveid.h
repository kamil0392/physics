#pragma once
enum class PrimitiveId {
    CUBE,
    SPHERE,
    PLANE
};
