#pragma once

#include "simulation/simulationentity.h"

class RocketEngine: public SimulationEntity
{
public:
    RocketEngine(Scene *scene, SimulationEntity *followedEntity, vec3 offset, glm::vec3 rotation, Qt::Key trigger);
};
