#include "cable.h"
#include "collision/collisionresolver.h"
#include "collision/interpenetrationresolver.h"
#include "collision/cubeshape.h"

Cable::Cable(Scene *scene, SimulationEntity *entity1, vec3 posOnEntity1, SimulationEntity *entity2, vec3 posOnEntity2, float length):
    length(length), entity1(entity1), posOnEntity1(posOnEntity1), entity2(entity2), posOnEntity2(posOnEntity2){

    this->scene = scene;
    shaderId = ShaderId::SIMPLE_MVP_SHADER;
    list<Shape*> shapeList = {new CubeShape(vec3(0.f, 0.f, 0.f), vec3(0.f, 0.f, 0.f), vec3(0.2f,0.2f,1.f))};
    complexShape = new ComplexShape(shapeList, vec3(0,0,0.f), vec3(0.f,0.f,0.f));
    collidable = false;
}

void Cable::recalcPosition(){
    vec3 cableBeginning = entity1->getPosition() + mat3(entity1->getRotationMatrix())*posOnEntity1;
    vec3 cableEnding = entity2->getPosition() + mat3(entity2->getRotationMatrix())*posOnEntity2;

    complexShape->setPosition(0.5f*(cableBeginning+cableEnding));

    vec3 cableLengthVec = cableEnding - cableBeginning;

    complexShape->setRotation(cableLengthVec);
    float currentLength = glm::length(cableLengthVec);
    (*(complexShape->getShapes()->begin()))->setScalingFrame(vec3(0.2,0.2,0.5*currentLength));
}

void Cable::checkCollision(){
    vec3 cableBeginning = entity1->getPosition() + mat3(entity1->getRotationMatrix())*posOnEntity1;
    vec3 cableEnding = entity2->getPosition() + mat3(entity2->getRotationMatrix())*posOnEntity2;

    vec3 cableLengthVec = cableEnding - cableBeginning;
    float currentLength = glm::length(cableLengthVec);

    if(currentLength > length){
        vec3 leverArm1InGlobalFrame = mat3(entity1->getRotationMatrix())*posOnEntity1;
        vec3 collisionPoint1TangentialVelInGlobalFrame;
        if(entity1->isMovable()){
            collisionPoint1TangentialVelInGlobalFrame = glm::cross(entity1->getPhysicsComponent()->getAngularVelocity(), leverArm1InGlobalFrame);
        }

        vec3 leverArm2InGlobalFrame = mat3(entity2->getRotationMatrix())*posOnEntity2;
        vec3 collisionPoint2TangentialVelInGlobalFrame;
        if(entity2->isMovable()){
            collisionPoint2TangentialVelInGlobalFrame = glm::cross(entity2->getPhysicsComponent()->getAngularVelocity(), leverArm2InGlobalFrame);
        }

        vec3 relativeVelocityInGlobalFrame;
        if(entity1->isMovable()){
            relativeVelocityInGlobalFrame = (entity1->getPhysicsComponent()->getLinearVelocity() + collisionPoint1TangentialVelInGlobalFrame);
        }
        if(entity2->isMovable()){
            relativeVelocityInGlobalFrame -= (entity2->getPhysicsComponent()->getLinearVelocity() + collisionPoint2TangentialVelInGlobalFrame);
        }
        if(entity1->isMovable() && entity2->isMovable()){
            CollisionResolver::resolveMovableMovable(glm::normalize(-cableLengthVec), relativeVelocityInGlobalFrame,
                                                   entity1, leverArm1InGlobalFrame,
                                                   entity2, leverArm2InGlobalFrame);
        } else if(entity1->isMovable()){
            CollisionResolver::resolveMovableStationary(glm::normalize(-cableLengthVec), relativeVelocityInGlobalFrame, entity1, leverArm1InGlobalFrame, entity2);
        } else if(entity2->isMovable()){
            CollisionResolver::resolveMovableStationary(glm::normalize(cableLengthVec), -relativeVelocityInGlobalFrame, entity2, leverArm2InGlobalFrame, entity1);
        }


        InterpenetrationResolver::linearResolution(glm::normalize(cableLengthVec),
                                                   currentLength - length,
                                                   entity1, entity2);
    }
}

void Cable::update(Context *context){
    checkCollision();
    recalcPosition();
}
