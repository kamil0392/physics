#include "rocketengine.h"
#include "collision/cubeshape.h"
#include "simulation/behaviour/follower.h"
#include "simulation/behaviour/thrustsource.h"

RocketEngine::RocketEngine(Scene *scene, SimulationEntity *followedEntity, glm::vec3 offset, vec3 rotation, Qt::Key trigger){
    this->scene = scene;
    shaderId = ShaderId::SIMPLE_MVP_SHADER;
    list<Shape*> shapeList = {new CubeShape(vec3(0.f, 0.f, 0.f), vec3(0.f, 0.f, 0.f), vec3(.5f,.5f,.1f))};
    complexShape = new ComplexShape(shapeList, vec3(0,0,0.f), vec3(0.f,0.f,0.f));

    addBehaviour(new Follower(followedEntity, offset, rotation));
    addBehaviour(new ThrustSource(followedEntity, 200, trigger));

    collidable = false;
}

