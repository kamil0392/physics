#pragma once

#include "simulation/simulationentity.h"
#include "glm/glm.hpp"

using glm::vec3;

class Spring: public SimulationEntity
{
public:
    Spring(Scene *scene, SimulationEntity *entity1, vec3 posOnEntity1, SimulationEntity *entity2, vec3 posOnEntity2, float k);

    void update(Context *context);
protected:
    void recalcPosition();
    void exertForce();


    float k;
    float baseLength;
    float currentLength;

    SimulationEntity *entity1;
    vec3 posOnEntity1;
    SimulationEntity *entity2;
    vec3 posOnEntity2;

};

