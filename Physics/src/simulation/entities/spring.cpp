#include "spring.h"
#include "collision/cubeshape.h"

Spring::Spring(Scene *scene, SimulationEntity *entity1, vec3 posOnEntity1, SimulationEntity *entity2, vec3 posOnEntity2, float k):
    k(k), entity1(entity1), posOnEntity1(posOnEntity1), entity2(entity2), posOnEntity2(posOnEntity2){

    this->scene = scene;
    shaderId = ShaderId::SIMPLE_MVP_SHADER;
    list<Shape*> shapeList = {new CubeShape(vec3(0.f, 0.f, 0.f), vec3(0.f, 0.f, 0.f), vec3(0.2f,0.2f,1.f))};
    complexShape = new ComplexShape(shapeList, vec3(0,0,0.f), vec3(0.f,0.f,0.f));

    vec3 springBeginning = entity1->getPosition() + mat3(entity1->getRotationMatrix())*posOnEntity1;
    vec3 springEnding = entity2->getPosition() + mat3(entity2->getRotationMatrix())*posOnEntity2;

    baseLength = glm::length(springBeginning - springEnding);
    collidable = false;
}


void Spring::recalcPosition(){
    vec3 springBeginning = entity1->getPosition() + mat3(entity1->getRotationMatrix())*posOnEntity1;
    vec3 springEnding = entity2->getPosition() + mat3(entity2->getRotationMatrix())*posOnEntity2;

    complexShape->setPosition(0.5f*(springBeginning+springEnding));

    vec3 springLengthVec = springEnding - springBeginning;

    complexShape->setRotation(springLengthVec);
    currentLength = glm::length(springLengthVec);
    (*(complexShape->getShapes()->begin()))->setScalingFrame(vec3(0.2,0.2,0.5*currentLength));
}

void Spring::exertForce(){
    vec3 springBeginning = entity1->getPosition() + mat3(entity1->getRotationMatrix())*posOnEntity1;
    vec3 springEnding = entity2->getPosition() + mat3(entity2->getRotationMatrix())*posOnEntity2;

    vec3 forceDir = glm::normalize(springEnding - springBeginning);
    vec3 force = k*(currentLength-baseLength)*forceDir;

    float colorChange = k*glm::abs(currentLength-baseLength)/30.f;
    if(colorChange > 1.f)
        colorChange = 1.f;
    complexShape->setColor(vec4(colorChange, 1.f-colorChange, 0.f, 1.f));

    if (entity1->isMovable()){
        entity1->getPhysicsComponent()->applyForce(force, mat3(entity1->getRotationMatrix())*posOnEntity1);
    } else if(entity2->isMovable()){
        entity2->getPhysicsComponent()->applyForce(-force, mat3(entity2->getRotationMatrix())*posOnEntity2);
    }
    if (entity2->isMovable()){
        entity2->getPhysicsComponent()->applyForce(-force, mat3(entity2->getRotationMatrix())*posOnEntity2);
    } else if(entity1->isMovable()){
        entity1->getPhysicsComponent()->applyForce(force, mat3(entity1->getRotationMatrix())*posOnEntity1);
    }
}

void Spring::update(Context *context){
    recalcPosition();
    exertForce();
}
