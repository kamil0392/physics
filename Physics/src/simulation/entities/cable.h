#pragma once

#include "simulation/simulationentity.h"
#include "glm/glm.hpp"

using glm::vec3;

class Cable: public SimulationEntity
{
public:
    Cable(Scene *scene, SimulationEntity *entity1, vec3 posOnEntity1, SimulationEntity *entity2, vec3 posOnEntity2, float length);

    void update(Context *context);
protected:
    void recalcPosition();
    void checkCollision();

    float length;

    SimulationEntity *entity1;
    vec3 posOnEntity1;
    SimulationEntity *entity2;
    vec3 posOnEntity2;

};
