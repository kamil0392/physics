#include "camera.h"
#include "input/inputhandler.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/constants.hpp>
#include <QKeyEvent>
#include <QDebug>

Camera::Camera()
{
    //position = vec3(21.5, 7.64, 22.6);
    //angleHor = -0.74f;
    //angleVert = 0.3f;
    position = vec3(0, 0, 8);
    angleHor = 0.f;
    angleVert = 0.0f;
}

void Camera::update(double delay){
    vec3 velocity(0.f, 0.f, 0.f);
    vec2 rotation(0.f, 0.f);
    float speedModifier = 1.f;

    if (InputHandler::getInstance()->getKeyState(Qt::Key_W)){
        velocity += vec3(0.f ,0.f, -1.f);
    }
    if (InputHandler::getInstance()->getKeyState(Qt::Key_S)){
        velocity += vec3(0.f ,0.f, 1.f);
    }
    if (InputHandler::getInstance()->getKeyState(Qt::Key_D)){
        velocity += vec3(1.f ,0.f, 0.0f);
    }
    if (InputHandler::getInstance()->getKeyState(Qt::Key_A)){
        velocity += vec3(-1.f ,0.f, 0.f);
    }
    if (InputHandler::getInstance()->getKeyState(Qt::Key_R)){
        velocity += vec3(0.f ,1.f, 0.0f);
    }
    if (InputHandler::getInstance()->getKeyState(Qt::Key_F)){
        velocity += vec3(0.f ,-1.f, 0.f);
    }

    if (InputHandler::getInstance()->getKeyState(Qt::Key_Q)){
        rotation += 0.3f*vec2(-1.f, 0.f);
    }
    if (InputHandler::getInstance()->getKeyState(Qt::Key_E)){
        rotation += 0.3f*vec2(1.f, 0.f);
    }
    if (InputHandler::getInstance()->getKeyState(Qt::Key_T)){
        rotation += 0.3f*vec2(0.f, -1.f);
    }
    if (InputHandler::getInstance()->getKeyState(Qt::Key_G)){
        rotation += 0.3f*vec2(0.f, 1.f);
    }

    if (InputHandler::getInstance()->getKeyState(Qt::Key_Shift)){
        speedModifier *= 5;
    }
    if(delay < 0.001)
        delay = 0.02;
    move(velocity * (float)delay * speed * speedModifier);
    rotate(rotation * (float)delay * speed * speedModifier);

}

void Camera::rotate(vec2 angles){
    angleHor += angles.x;
    angleVert += angles.y;
}

void Camera::move(vec3 moveVec){
    position += glm::rotateY(glm::rotateX(moveVec, -angleVert), -angleHor);
}

mat4 Camera::getCameraMatrix(){
    mat4 rotated = glm::rotate(mat4(1.f), angleVert, vec3(1.f, 0.f, 0.f));
    rotated = glm::rotate(rotated, angleHor, vec3(0.f, 1.f, 0.f));
    return glm::translate(rotated, -position);
}
