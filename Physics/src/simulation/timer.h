#pragma once
#include <chrono>

using namespace std::chrono;

class Timer
{
public:
    Timer();

    double getDiffTime();

protected:
    high_resolution_clock::time_point lastMeasuredTime;
};
