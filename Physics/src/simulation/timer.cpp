#include "timer.h"

Timer::Timer()
{
    lastMeasuredTime = high_resolution_clock::now();
}

double Timer::getDiffTime(){
    high_resolution_clock::time_point curTime = high_resolution_clock::now();
    double diff = duration_cast<microseconds>(curTime-lastMeasuredTime).count() / 1000000.f;
    lastMeasuredTime = curTime;
    return diff;
}



