#include "context.h"

Context::Context()
{

}

void Context::setCamera(Camera *camera){
    this->camera = camera;
}

mat4 Context::getCameraMatrix(){
    return camera->getCameraMatrix();
}

void Context::setDelay(float delay){
    this->delay = delay;
}

float Context::getDelay(){
    return delay;
}


void Context::setMatrixProvider(MatrixProvider *matrixProvider){
    this->matrixProvider = matrixProvider;
}

mat4 Context::getPerspectiveMatrix(){
    return matrixProvider->getPerspectiveMatrix();
}

void Context::setRenderProvider(RenderProvider* renderProvider){
    this->renderProvider = renderProvider;
}

RenderProvider* Context::getRenderProvider(){
    return renderProvider;
}
