#include "fpscounter.h"

FpsCounter::FpsCounter()
{
    for(int i=0;i<500;i++)
        lastFramesDurations.push_back(0.f);
}


void FpsCounter::update(double delay){
    lastFramesDurations.push_back((float)delay);
    lastFramesDurations.pop_front();
}

double FpsCounter::getFPSNumber(){
    float delaySum = 0.f;
    for(auto delay : lastFramesDurations){
        delaySum += delay;
    }

    return lastFramesDurations.size()/delaySum; // 1/avg_delay
}
