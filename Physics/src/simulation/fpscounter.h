#pragma once
#include <list>

using std::list;

class FpsCounter
{
public:
    FpsCounter();
    void update(double delay);
    double getFPSNumber();
protected:
    list<float> lastFramesDurations;
};
