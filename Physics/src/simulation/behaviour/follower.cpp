#include "follower.h"
#include "helpers/algebra.h"

using glm::mat3;

Follower::Follower(SimulationEntity *followedEntity, glm::vec3 offset, vec3 rotation): followedEntity(followedEntity), offset(offset){
    vec3 rotationInRadians = vec3(glm::radians(rotation.x), glm::radians(rotation.y), glm::radians(rotation.z));
    rotationMatrix = Algebra::createRotationMatrix(rotationInRadians);
}

void Follower::onCollision(SimulationEntity *collisionParticipant){

}

void Follower::update(){
    simulationEntity->getComplexShape()->setPosition(followedEntity->getPosition() +
                                                     mat3(followedEntity->getRotationMatrix()) * offset);
    simulationEntity->getComplexShape()->setRotationMatrix(followedEntity->getRotationMatrix() * rotationMatrix);
}
