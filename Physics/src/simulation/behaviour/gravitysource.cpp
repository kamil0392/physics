#include "gravitysource.h"

GravitySource::GravitySource(){
}

void GravitySource::onCollision(SimulationEntity *collisionParticipant){
    if (collisionParticipant->isCollidable()){
        vec3 forceDir = simulationEntity->getPosition() - collisionParticipant->getPosition();
        forceDir /= glm::length(forceDir);
        collisionParticipant->getPhysicsComponent()->applyForce(5*collisionParticipant->getComplexShape()->getVolume()*forceDir, vec3(0,0,0));
    }
}
