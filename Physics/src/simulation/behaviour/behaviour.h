#pragma once

#include "simulation/simulationentity.h"

class Behaviour
{
public:
    Behaviour();
    virtual void setSimulationEntity(SimulationEntity *simulationEntity);
    virtual void onCollision(SimulationEntity *collisionParticipant) = 0;
    virtual void update() {}
protected:
    SimulationEntity *simulationEntity;
};

