#pragma once

#include "simulation/behaviour/behaviour.h"

using glm::vec4;

class ThrustSource : public Behaviour
{
public:
    ThrustSource(SimulationEntity *forcingEntity, float power, Qt::Key trigger);
    void setSimulationEntity(SimulationEntity *simulationEntity);
    virtual void onCollision(SimulationEntity *collisionParticipant);
    virtual void update();

protected:
    SimulationEntity *forcingEntity;
    float power;
    vec4 color;
    Qt::Key trigger;
};

