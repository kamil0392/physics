#pragma once

#include "simulation/behaviour/behaviour.h"
#include "glm/glm.hpp"

using glm::vec3;

class SimulationEntity;

class Follower : public Behaviour
{
public:
    Follower(SimulationEntity *followedEntity, vec3 offset, glm::vec3 rotation);

    virtual void onCollision(SimulationEntity *collisionParticipant);
    virtual void update();

protected:
    SimulationEntity *followedEntity;
    vec3 offset;
    mat4 rotationMatrix;
};

