#include "thrustsource.h"
#include "input/inputhandler.h"

ThrustSource::ThrustSource(SimulationEntity *forcingEntity, float power, Qt::Key trigger): forcingEntity(forcingEntity), power(power), trigger(trigger){
}


void ThrustSource::setSimulationEntity(SimulationEntity *simulationEntity){
    Behaviour::setSimulationEntity(simulationEntity);
    color = simulationEntity->getComplexShape()->getColor();
}

void ThrustSource::onCollision(SimulationEntity *collisionParticipant){

}

void ThrustSource::update(){
    if (InputHandler::getInstance()->getKeyState(trigger)){
        simulationEntity->getComplexShape()->setColor(vec4(1,0,0,1));
        vec3 force = mat3(simulationEntity->getRotationMatrix()) * vec3(0,0,1) * power;
        forcingEntity->getPhysicsComponent()->applyForce(force, simulationEntity->getPosition() - forcingEntity->getPosition());
    } else {
        simulationEntity->getComplexShape()->setColor(color);
    }
}
