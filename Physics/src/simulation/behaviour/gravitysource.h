#pragma once

#include "simulation/behaviour/behaviour.h"

class GravitySource : public Behaviour
{
public:
    GravitySource();

    void onCollision(SimulationEntity *collisionParticipant);
};
