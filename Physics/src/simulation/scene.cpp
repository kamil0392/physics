#include "scene.h"
#include "render/rendercomponent.h"
#include "render/VertexBufferFactory.h"
#include "render/VertexAttribBuffer.h"
#include "shaders/ShaderId.h"
#include "helpers/objloader.h"
#include "collision/complexshape.h"
#include "collision/collisiondetector.h"
#include "collision/sphereshape.h"
#include "collision/planeshape.h"
#include "collision/cubeshape.h"
#include <ctime>
#include <QDebug>
#include "simulation/timer.h"
#include "simulation/fpscounter.h"
#include "simulation/behaviour/gravitysource.h"
#include <stdio.h>
#include "simulation/entities/spring.h"
#include "simulation/entities/cable.h"
#include "simulation/entities/rod.h"
#include "simulation/entities/rocketengine.h"
#include "input/inputhandler.h"
#include "helpers/algebra.h"

void Scene::createScene(int scenarioNumber){
    for(auto entity : entityMap){
        delete entity.second;
    }
    entityMap.clear();

    // Add some primitives to simulation for testing purpose
    list<Shape*> shapeList;
    PhysicsComponent *physicsComponent;
    SimulationEntity *simulationEntity;

    switch(scenarioNumber){
    case 1:
        shapeList = {new SphereShape(vec3(-2.f, 1.f, 0.f), vec3(0.f, 0.f, 0.f), 1.f),
                     new SphereShape(vec3(2.f, 1.f, 0.f), vec3(0.f, 0.f, 0.f), 1.f),
                     new CubeShape(vec3(0.f, 1.f, 0.f), vec3(0.f, 0.f, 0.f), vec3(1.0f, 1.0f, 1.0f))};
        physicsComponent = new PhysicsComponent(1.f);
        simulationEntity = new SimulationEntity(
                    this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                    new ComplexShape(shapeList, vec3(0.f,0.0f,0), vec3(0,0,0), vec4(1,0.,0., 1.f)),
                    physicsComponent);
        addEntity(simulationEntity);

        shapeList = {new SphereShape(vec3(-2.f, 1.f, 7.f), vec3(0.f, 0.f, 0.f), 1.f)};
        physicsComponent = new PhysicsComponent(1.f);
        physicsComponent->setVelocity(vec3(0.f,0.f,-4.f));
        simulationEntity = new SimulationEntity(
                    this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                    new ComplexShape(shapeList, vec3(0.f,0.0f,0), vec3(0,0,0), vec4(1,0.5,0.2, 1.f)),
                    physicsComponent);
        addEntity(simulationEntity);

        shapeList = {new CubeShape(vec3(-26.5f, 0.f, 0.f), vec3(0.f, 0.f, 0.f), vec3(1.0f,1.0f,1.0f))};
        physicsComponent = new PhysicsComponent(1.f);
        simulationEntity = new SimulationEntity(
                    this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                    new ComplexShape(shapeList, vec3(0.f,0.0f,0), vec3(0,0,0), vec4(0.3,0.5,0.2, 1.)),
                    physicsComponent);
        addEntity(simulationEntity);

        shapeList = {new SphereShape(vec3(0.f, 0.f, 0.f), vec3(0.f, 0.f, 0.f), 10.f)};
        physicsComponent = new PhysicsComponent(1.f);
        simulationEntity = new SimulationEntity(
                            this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                            new ComplexShape(shapeList, vec3(-15,0,0), vec3(0.f,0.f,0.f), vec4(0.4,0.5,1., 0.5)),
                            physicsComponent);
        simulationEntity->addBehaviour(new GravitySource());
        simulationEntity->setCollidability(false);
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0, 0.f, 0.f), vec3(90.f, 0.f, 0.f), vec3(50, 50, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0,0,0), vec3(0,0,0.f), vec4(1.f, 0.5f, 0.4f, 0.2f), 0.f));
        addEntity(simulationEntity);
        break;
    case 0:
        shapeList = {new SphereShape(vec3(-14.f, 13.f, -3.f), vec3(0.f, 0.f, 0.f), 3.f)};
        physicsComponent = new PhysicsComponent(1.f);
        physicsComponent->setVelocity(vec3(8.f,0.f,0.f));
        simulationEntity = new SimulationEntity(
                    this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                    new ComplexShape(shapeList, vec3(0.f,0.0f,0), vec3(0,0,0), vec4(1,1,0.2, 1.)),
                    physicsComponent);
        addEntity(simulationEntity);

        for(float i=0.f; i < 8.f; i+= 2.5f){
            for(float j=0.f;j<8;j+=2.5f){
                for(float k=0.f;k<3;k+=2.5f){
                    shapeList = {new CubeShape(vec3(i, 10.f+j, -4.f+k), vec3(0.f, 0.f, 0.f), vec3(1.f, 1.f, 1.f))};
                    physicsComponent = new PhysicsComponent(1.f);
                    simulationEntity = new SimulationEntity(
                                this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                                new ComplexShape(shapeList, vec3(0.f,0.f,0.f), vec3(0,0,0), vec4(1,0.1,0.2,1.)),
                                physicsComponent);
                    addEntity(simulationEntity);
                }
            }
        }
        break;
    case 2:
    {
        shapeList = {new PlaneShape(vec3(0, 0, 0.f), vec3(90.f, 0.f, 0.f), vec3(50, 50, 1.f))};
        SimulationEntity* planeEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0,5.f,0), vec3(0,0,0.f), vec4(1.f, 0.5f, 0.4f, 0.2f), 0.f));
        addEntity(planeEntity);

        for(int i=0; i<3; i++){
            shapeList = {new SphereShape(vec3(i*3.f,7.f, -4.f), vec3(0.f, 0.f, 0.f), 1.f)};
            PhysicsComponent *physicsComponent = new PhysicsComponent(1.f);
            SimulationEntity *simulationEntity2 = new SimulationEntity(
                        this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                        new ComplexShape(shapeList, vec3(0,0,0), vec3(0,0,0)),
                        physicsComponent);
            addEntity(simulationEntity2);

            SimulationEntity *spring;

            shapeList = {new SphereShape(vec3(i*3.f, -15.f, -4.f), vec3(0.f, 0.f, 0.f), 1.f)};
            physicsComponent = new PhysicsComponent(1.f);
            simulationEntity = new SimulationEntity(
                        this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                        new ComplexShape(shapeList, vec3(0,0,0), vec3(0,0,0)),
                        physicsComponent);
            addEntity(simulationEntity);

            switch (i) {
            case 0:
                spring = new Cable(this, simulationEntity, vec3(0.f,0.f,0.f), simulationEntity2, vec3(0.f,0.f,0.f), 30.f);
                break;
            case 1:
                spring = new Spring(this, simulationEntity, vec3(0.f,0.f,0.f), simulationEntity2, vec3(0.f,0.f,0.f), 12.f);
                break;
            case 2:
                spring = new Rod(this, simulationEntity, vec3(0.f,0.f,0.f), simulationEntity2, vec3(0.f,0.f,0.f), 30.f);
                //to fix, not sure how it should work
                break;
            }

            addEntity(spring);
        }

        shapeList = {new SphereShape(vec3(0.f, -100.f, 0.f), vec3(0.f, 0.f, 0.f), 1000.f)};
        physicsComponent = new PhysicsComponent(1.f);
        simulationEntity = new SimulationEntity(
                            this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                            new ComplexShape(shapeList, vec3(0,0,0), vec3(0.f,0.f,0.f), vec4(0.4,0.5,1., 0.9)),
                            physicsComponent);
        simulationEntity->addBehaviour(new GravitySource());
        simulationEntity->setCollidability(false);
        addEntity(simulationEntity);

        break;
    }
    case 3:// tarcie 1 complex z 2 kulek i plane
    {
        shapeList = {new PlaneShape(vec3(0, 0.f, 0.f), vec3(90.f, 0.f, 30.f), vec3(50, 50, 1.f))};
        SimulationEntity* planeEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0,-10.f,0), vec3(0,0,0.f), vec4(1.f, 0.5f, 0.4f, 0.2f), 0.2f));
        addEntity(planeEntity);

        shapeList = {new SphereShape(vec3(0,0,0), vec3(0.f, 0.f, 0.f), 5.f),
                     new SphereShape(vec3(0,10,0), vec3(0.f, 0.f, 0.f), 5.f)};
        physicsComponent = new PhysicsComponent(1.f);
        physicsComponent->setVelocity(vec3(0, -5.f, 0));
        simulationEntity = new SimulationEntity(
                    this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                    new ComplexShape(shapeList, vec3(6,15,-15.f), vec3(0,0,0), vec4(0.7,0.4,.4, 1.), 0.2f),
                    physicsComponent);
        addEntity(simulationEntity);

        shapeList = {new SphereShape(vec3(500,500,500), vec3(0.f, 0.f, 0.f), 1.f)};
        physicsComponent = new PhysicsComponent(1.f);
        physicsComponent->setVelocity(vec3(0, -5.f, 0));
        simulationEntity = new SimulationEntity(
                    this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                    new ComplexShape(shapeList, vec3(6,15,-15.f), vec3(0,0,0), vec4(0.7,0.4,.4, 1.), 0.2f),
                    physicsComponent);
        addEntity(simulationEntity);

        shapeList = {new SphereShape(vec3(-5.f, -25.f, 0.f), vec3(0.f, 0.f, 0.f), 30.f)};
        physicsComponent = new PhysicsComponent(1.f);
        simulationEntity = new SimulationEntity(
                            this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                            new ComplexShape(shapeList, vec3(-15,0,0), vec3(0.f,0.f,0.f), vec4(0.4,0.5,1., 0.5)),
                            physicsComponent);
        simulationEntity->addBehaviour(new GravitySource());
        simulationEntity->setCollidability(false);
        addEntity(simulationEntity);

        break;
    }
    case 4:
    {
        vec3 boxSize = vec3(0.2f, 0.3f, 0.2f);
        float basePlaneSize = 100.f; //defined in renderprovider
        float halfSize = basePlaneSize / 2.f;

        int lowEnd = -2;
        int topEnd = 3;
        float speed = 5;

        for(int x=lowEnd; x<topEnd; x++){
            for(int y=lowEnd; y<topEnd; y++){
                for(int z=lowEnd; z<topEnd; z++){
                    shapeList = {new CubeShape(vec3(4.f*x, 4.f*y, 4.f*z), vec3(0.f, 0.f, 0.f), vec3(.5f, .5f, .5f))};
                    physicsComponent = new PhysicsComponent(1.f);
                    physicsComponent->setVelocity(vec3(-speed*x,-speed*y,-speed*z));
                    simulationEntity = new SimulationEntity(
                                this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                                new ComplexShape(shapeList, vec3(0.f,0.0f,0), vec3(0,0,0), vec4(1,0.5,0.2, 1.f)),
                                physicsComponent);
                    addEntity(simulationEntity);
                }
            }
        }

        shapeList = {new PlaneShape(vec3(0.f, 0.f, 0.f), vec3(0.f, -90.f, 0.f), vec3(boxSize.y, boxSize.z, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(-halfSize*boxSize.x,0,0), vec3(0,0,0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f, 0.f, 0.f), vec3(0.f, 90.f, 0.f), vec3(boxSize.y, boxSize.z, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(halfSize*boxSize.x,0,0), vec3(0,0,0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f, 0.f, 0.f), vec3(180.f, 0.f, 0.f), vec3(boxSize.x, boxSize.z, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0,0,-halfSize*boxSize.y), vec3(0,0,0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f, 0.f, 0.f), vec3(0.f, 0.f, 0.f), vec3(boxSize.x, boxSize.z, 1.f))};//front
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0,0,halfSize*boxSize.y), vec3(0.f, 0.f, 0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f, 0.f, 0.f), vec3(90.f, 0.f, 0.f), vec3(boxSize.x, boxSize.y, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0,-halfSize*boxSize.z,0), vec3(0.f, 0.f, 0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f,0.f, 0.f), vec3(-90.f, 0.f, 0.f), vec3(boxSize.x, boxSize.y, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0, halfSize*boxSize.z,0), vec3(0.f, 0.f, 0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        break;
    }
    case 5:
    {
        vec3 boxSize = vec3(0.2f, 0.3f, 0.2f);
        float basePlaneSize = 100.f; //defined in renderprovider
        float halfSize = basePlaneSize / 2.f;

        shapeList = {new CubeShape(vec3(0, 2, 0), vec3(0.f, 0.f, 0.f), vec3(1, 1, 1)),
                     new CubeShape(vec3(0, -2, 0), vec3(0.f, 0.f, 0.f), vec3(1, 1, 1)),
                     new SphereShape(vec3(0, 0, -2), vec3(0.f, 0.f, 0.f), 3)};
        physicsComponent = new PhysicsComponent(1.f);
        simulationEntity = new SimulationEntity(
                    this, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                    new ComplexShape(shapeList, vec3(0.f,0.0f,0), vec3(0,0,0), vec4(1,0.5,0.2, 1.f)),
                    physicsComponent);
        addEntity(simulationEntity);

        SimulationEntity *followedEntity = simulationEntity;

        simulationEntity = new RocketEngine(this, followedEntity, vec3(0,2,2.7), vec3(0,180,0), Qt::Key_V);
        addEntity(simulationEntity);
        simulationEntity = new RocketEngine(this, followedEntity, vec3(0,-2,2.7), vec3(0,180,0), Qt::Key_B);
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f, 0.f, 0.f), vec3(0.f, -90.f, 0.f), vec3(boxSize.y, boxSize.z, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(-halfSize*boxSize.x,0,0), vec3(0,0,0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f, 0.f, 0.f), vec3(0.f, 90.f, 0.f), vec3(boxSize.y, boxSize.z, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(halfSize*boxSize.x,0,0), vec3(0,0,0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f, 0.f, 0.f), vec3(180.f, 0.f, 0.f), vec3(boxSize.x, boxSize.z, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0,0,-halfSize*boxSize.y), vec3(0,0,0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f, 0.f, 0.f), vec3(0.f, 0.f, 0.f), vec3(boxSize.x, boxSize.z, 1.f))};//front
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0,0,halfSize*boxSize.y), vec3(0.f, 0.f, 0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f, 0.f, 0.f), vec3(90.f, 0.f, 0.f), vec3(boxSize.x, boxSize.y, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0,-halfSize*boxSize.z,0), vec3(0.f, 0.f, 0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        shapeList = {new PlaneShape(vec3(0.f,0.f, 0.f), vec3(-90.f, 0.f, 0.f), vec3(boxSize.x, boxSize.y, 1.f))};
        simulationEntity = new SimulationEntity(
                            this, ShaderId::UNIFORM_NORMAL_MVP,
                            new ComplexShape(shapeList, vec3(0, halfSize*boxSize.z,0), vec3(0.f, 0.f, 0.f), vec4(0.1f, 0.5f, 0.4f, 0.2f), 0.5f));
        addEntity(simulationEntity);

        break;
    }
    }
    broadPhaser.init(entityMap, this);
}

void Scene::init(){
    glClearColor(0.4f,0.7f,0.3f,5.f);
    //createScene(42);
}

void Scene::update(Context *context){
    static Timer timer;
    static FpsCounter fps;
    timer.getDiffTime();
    camera.update(context->getDelay());
    context->setCamera(&camera);

    if(InputHandler::getInstance()->getKeyState(Qt::Key_0)){
        createScene(0);
    }
    if(InputHandler::getInstance()->getKeyState(Qt::Key_1)){
        createScene(1);
    }
    if(InputHandler::getInstance()->getKeyState(Qt::Key_2)){
        createScene(2);
    }
    if(InputHandler::getInstance()->getKeyState(Qt::Key_3)){
        createScene(3);
    }
    if(InputHandler::getInstance()->getKeyState(Qt::Key_4)){
        createScene(4);
    }
    if(InputHandler::getInstance()->getKeyState(Qt::Key_5)){
        createScene(5);
    }

    for (auto entity : entityMap){
        entity.second->update(context);
    }

    //collision detection loop
    removeEntity(broadPhaser.getDebugEntityId());
    set<set<SimulationEntity*>> collisionEntities = broadPhaser.broadPhase();
    for(auto pair : collisionEntities){
        CollisionDetector::detectCollision(*pair.begin(), *(++pair.begin()));
    }

/*    for(auto entityIterator = entityMap.begin(); entityIterator != entityMap.end(); entityIterator++){
        for(auto secondEntityIterator = std::next(entityIterator); secondEntityIterator != entityMap.end(); secondEntityIterator++){
            CollisionDetector::detectCollision(entityIterator->second, secondEntityIterator->second);
        }
    }
*/
    fps.update(timer.getDiffTime());
    qDebug()<<fps.getFPSNumber();
}

void Scene::render(Context *context){
    glClear(GL_COLOR_BUFFER_BIT);
    for (auto entity : entityMap)
    {
        entity.second->render(context);
    }
}

int Scene::addEntity(SimulationEntity *entity){
    int newId = 0;
    bool isIdFree = false;
    while(!isIdFree){
        try {
            entityMap.at(newId);
            newId++;
        } catch(const std::out_of_range& exc){
            isIdFree = true;
        }
    }
    entityMap[newId] = entity;
    return newId;
}

SimulationEntity *Scene::getEntity(int id){
    return entityMap.at(id);
}

bool Scene::removeEntity(int id){
    return entityMap.erase(id);
}
