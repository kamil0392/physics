#pragma once

#include "global.h"
#include <glm/glm.hpp>

using glm::vec2;
using glm::vec3;
using glm::mat4;

class TEST_COMMON_DLLSPEC Camera
{
public:
    Camera();

    void rotate(vec2 angles);
    void move(vec3 moveVec);

    void update(double delay);

    mat4 getCameraMatrix();
protected:
    float speed = 5.f;
    vec3 position;
    float angleHor, angleVert;
};
