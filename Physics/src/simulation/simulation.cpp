#include "simulation.h"
#include <QDebug>
#include "input/inputhandler.h"

Simulation::Simulation(){
}

void Simulation::init() {
    InputHandler::init();

    renderProvider.init();
    scene.init();
}

void Simulation::tick(){
    if (InputHandler::getInstance()->getKeyState(Qt::Key_Escape)){
        exit(0);
    }

    static float nextD = 0.02;

    if(InputHandler::getInstance()->getKeyState(Qt::Key_Space)){
        if(nextD > 0.01)
            nextD = 0;
        else
            nextD = 0.02;
    }

    double delay = timer.getDiffTime();
    //fpsCounter.update(delay);
    //if (delay>0.05)
        delay = nextD; //for debug
    //qDebug() << fpsCounter.getFPSNumber() << endl;
    context.setDelay(delay);
    context.setMatrixProvider(&matrixProvider);
    context.setRenderProvider(&renderProvider);

    scene.update(&context);

    scene.render(&context);
}
