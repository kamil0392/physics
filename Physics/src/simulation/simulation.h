#pragma once

#include "global.h"
#include <QOpenGLFunctions_1_0>
#include <QOpenGLFunctions_4_0_Core>

#include "shaders/ShaderProvider.h"
#include "simulation/scene.h"
#include "render/matrixprovider.h"
#include "simulation/timer.h"
#include "simulation/fpscounter.h"
#include "simulation/context.h"
#include "render/renderprovider.h"

class OpenGLVersionTest: public QOpenGLFunctions_1_0
{
public:
    QString version()
    {
        initializeOpenGLFunctions();
        return (char *)glGetString(GL_VERSION);
    }
};

class TEST_COMMON_DLLSPEC Simulation: public QOpenGLFunctions_4_0_Core
{
public:
    Simulation();

    void init();
    void tick();

    MatrixProvider* getMatrixProvider() { return &matrixProvider; }

protected:
    RenderProvider renderProvider;
    MatrixProvider matrixProvider;
    FpsCounter fpsCounter;
    Timer timer;
    Scene scene;
    Context context;
};
