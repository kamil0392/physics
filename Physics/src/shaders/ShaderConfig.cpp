#include "ShaderConfig.h"
#include <string.h>
#include <string>
#include <iostream>

using namespace std;

ShaderConfig::ShaderConfig(const char* vertSrcPath, const char* fragSrcPath, list<UniformName>* usedUniformMatrices,
	list<UniformName>* usedUniformVectors, list<UniformName>* usedUniformScalars) {
	this->vertSrcPath = new char[strlen(vertSrcPath)+1];
	strcpy_s(this->vertSrcPath, strlen(vertSrcPath)+1, vertSrcPath);

	this->fragSrcPath = new char[strlen(fragSrcPath)+1];
	strcpy_s(this->fragSrcPath, strlen(fragSrcPath)+1, fragSrcPath);

	this->usedUniformMatrices = usedUniformMatrices;
	this->usedUniformVectors = usedUniformVectors;
	this->usedUniformScalars = usedUniformScalars;

}

ShaderConfig::~ShaderConfig() {
	delete[] vertSrcPath;
	delete[] fragSrcPath;
	delete usedUniformMatrices;
	delete usedUniformVectors;
	delete usedUniformScalars;
}

list<UniformName>* ShaderConfig::getMatricesList() {
	return usedUniformMatrices;
}
list<UniformName>* ShaderConfig::getVectorsList() {
	return usedUniformVectors;
}
list<UniformName>* ShaderConfig::getScalarsList() {
	return usedUniformScalars;
}

const char* ShaderConfig::getVertSrcPath() {
	return vertSrcPath;
}
const char* ShaderConfig::getFragSrcPath() {
	return fragSrcPath;
}