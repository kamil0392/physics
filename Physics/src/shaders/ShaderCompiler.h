#pragma once
#include <QOpenGLFunctions_4_0_Core>

class ShaderCompiler : public QOpenGLFunctions_4_0_Core{
public:
	ShaderCompiler();
	virtual ~ShaderCompiler();

	GLuint compileShadersPair(const char *vertexSourceFileName, const char *fragmentSourceFileName);
};
