#pragma once
#include <map>
#include "Shaderid.h"
#include "Shader.h"
#include "UniformName.h"

using namespace std;
using glm::mat4;
using glm::vec4;

class ShaderProvider {
public:
	ShaderProvider();
	virtual ~ShaderProvider();

    void init();
    Shader *getShader(ShaderId shaderId);

protected:
	map<ShaderId, Shader*> shaders;
};
