#include "ShaderCompiler.h"
#include "helpers/FileLoader.h"
#include <iostream>
#include <stdio.h>

#include <qdebug.h>

using namespace std;

ShaderCompiler::ShaderCompiler() {
    initializeOpenGLFunctions();
}

ShaderCompiler::~ShaderCompiler() {
}

GLuint ShaderCompiler::compileShadersPair(const char *vertexSourceFileName, const char *fragmentSourceFileName) {
	GLuint vertexShader, fragmentShader;
	char *vertexSource;
	char *fragmentSource;


	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	FileLoader fileLoader;

	vertexSource = fileLoader.loadFile(vertexSourceFileName);
	fragmentSource = fileLoader.loadFile(fragmentSourceFileName);

	if (vertexSource == NULL || fragmentSource == NULL) {
        cout << "Shader creation has failed." << endl;
		return -1;
	}

	glShaderSource(vertexShader, 1, (const char **)&vertexSource, NULL);
	glShaderSource(fragmentShader, 1, (const char **)&fragmentSource, NULL);

	delete vertexSource;
	delete fragmentSource;

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);


	GLint testVal;

	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &testVal);
	if (testVal == GL_FALSE) {
		char infoLog[1024];
		glGetShaderInfoLog(vertexShader, 1024, NULL, infoLog);

        std::cout << "Vertex shader compilation has failed. Error:\n" << infoLog << "\n";

		throw 6;
	}

	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &testVal);
	if (testVal == GL_FALSE) {
		char infoLog[1024];
		glGetShaderInfoLog(fragmentShader, 1024, NULL, infoLog);

        std::cout << "Fragment shader compilation has failed. Error:\n" << infoLog << "\n";

		throw 7;
	}

	GLuint program;

	program = glCreateProgram();

	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);

	glLinkProgram(program);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glGetProgramiv(program, GL_LINK_STATUS, &testVal);
	if (testVal == GL_FALSE) {
		char infoLog[1024];
		glGetProgramInfoLog(program, 1024, NULL, infoLog);

        std::cout << "Shader's consolidation has failed. Error:\n" << infoLog << "\n";

		throw 8;
	}

    std::cout << "Shaders compilations (\"" << vertexSourceFileName << "\" and \"" << fragmentSourceFileName << "\") were successful.\n";

	return program;
}
