#pragma once
#include <QOpenGLFunctions_4_0_Core>

#include <map>
#include <list>

#include <glm/glm.hpp>

#include "UniformName.h"
#include "ShaderConfig.h"

using namespace std;
using glm::mat4;
using glm::vec4;

class Shader : public QOpenGLFunctions_4_0_Core{
public:
	Shader(ShaderConfig* config);
	virtual ~Shader();

	void useShader();
    virtual bool setMatrice(UniformName matrixName, mat4 value);
    virtual bool setVector(UniformName vectorName, vec4 value);
	virtual bool setScalar(UniformName scalarName, float value);
	virtual bool setScalar(UniformName scalarName, int value);

protected:
	map<UniformName, GLuint> uniformMatrices;
	map<UniformName, GLuint> uniformVectors;
	map<UniformName, GLuint> uniformScalars;
	GLuint program;
};
