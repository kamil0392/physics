#include "ShaderProvider.h"
#include "ShaderConfigProvider.h"
#include "ShaderId.h"

ShaderProvider::ShaderProvider() {
}

ShaderProvider::~ShaderProvider() {
	for (map<ShaderId, Shader*>::iterator iter = shaders.begin(); iter != shaders.end(); ++iter)
	{
		delete iter->second;
	}
	shaders.clear();
}

void ShaderProvider::init(){
    ShaderConfigProvider shaderConfigManager;
    shaders[ShaderId::SIMPLE_SHADING_MVP_SHADER] = new Shader(shaderConfigManager.getConfig(ShaderId::SIMPLE_SHADING_MVP_SHADER));
    shaders[ShaderId::SIMPLE_MVP_SHADER] = new Shader(shaderConfigManager.getConfig(ShaderId::SIMPLE_MVP_SHADER));
    shaders[ShaderId::UNIFORM_NORMAL_MVP] = new Shader(shaderConfigManager.getConfig(ShaderId::UNIFORM_NORMAL_MVP));
}

Shader *ShaderProvider::getShader(ShaderId shaderId){
    return shaders[shaderId];
}
