#pragma once
#include <list>
#include "UniformName.h"

using namespace std;

class ShaderConfig {
public:
	ShaderConfig(const char* vertSrcPath, const char* fragSrcPath, list<UniformName>* usedUniformMatrices,
		list<UniformName>* usedUniformVectors, list<UniformName>* usedUniformScalars);
	virtual ~ShaderConfig();

	list<UniformName>* getMatricesList();
	list<UniformName>* getVectorsList();
	list<UniformName>* getScalarsList();

	const char* getVertSrcPath();
	const char* getFragSrcPath();

protected:
	char* vertSrcPath;
	char* fragSrcPath;
	list<UniformName>* usedUniformMatrices;
	list<UniformName>* usedUniformVectors;
	list<UniformName>* usedUniformScalars;
};
