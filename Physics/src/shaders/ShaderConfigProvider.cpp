#include "ShaderConfigProvider.h"
#include "UniformName.h"
#include "ShaderId.h"
#include <list>
#include <stddef.h>

ShaderConfigProvider::ShaderConfigProvider() {//TODO dodac tu ladowanie z pliku
    // Simple shader with ambient and diffuse
    list<UniformName>* matrixUniforms = new list<UniformName>();
    matrixUniforms->push_back(UniformName::MVP_MATRIX);

    list<UniformName>* vectorUniforms = new list<UniformName>();
    vectorUniforms->push_back(UniformName::ENTITY_COLOR);

    ShaderConfig* config = new ShaderConfig("..\\..\\Physics\\simple_shading_mvp.vert",
                                            "..\\..\\Physics\\simple_shading_mvp.frag",
                                            matrixUniforms, vectorUniforms, NULL);

    configs[ShaderId::SIMPLE_SHADING_MVP_SHADER] = config;

    // Simple shader without real shading
    matrixUniforms = new list<UniformName>();
    matrixUniforms->push_back(UniformName::MVP_MATRIX);

    vectorUniforms = new list<UniformName>();
    vectorUniforms->push_back(UniformName::ENTITY_COLOR);
    config = new ShaderConfig("..\\..\\Physics\\simple_mvp.vert",
                              "..\\..\\Physics\\simple_mvp.frag",
                              matrixUniforms, vectorUniforms, NULL);

    configs[ShaderId::SIMPLE_MVP_SHADER] = config;

    // Simple shader with ambient and diffuse using uniform normal
    matrixUniforms = new list<UniformName>();
    matrixUniforms->push_back(UniformName::MVP_MATRIX);

    vectorUniforms = new list<UniformName>();
    vectorUniforms->push_back(UniformName::ENTITY_COLOR);
    vectorUniforms->push_back(UniformName::NORMAL);
    config = new ShaderConfig("..\\..\\Physics\\uniform_normal_mvp.vert",
                              "..\\..\\Physics\\uniform_normal_mvp.frag",
                              matrixUniforms, vectorUniforms, NULL);

    configs[ShaderId::UNIFORM_NORMAL_MVP] = config;

}

ShaderConfigProvider::~ShaderConfigProvider() {
	for (map<ShaderId, ShaderConfig*>::iterator iter = configs.begin(); iter != configs.end(); ++iter)
	{
		delete iter->second;
	}
}


ShaderConfig* ShaderConfigProvider::getConfig(ShaderId shaderId) {
	return configs[shaderId];
}
