#pragma once
enum class ShaderId {
    SIMPLE_SHADING_MVP_SHADER,
    WINDOW_SHADER,
    SIMPLE_MVP_SHADER,
    UNIFORM_NORMAL_MVP,
    NONE
};
