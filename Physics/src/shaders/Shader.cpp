#include "Shader.h"
#include "ShaderCompiler.h"
#include <stdio.h>
#include <glm/gtc/type_ptr.hpp>
#include <map>
#include <utility>

Shader::Shader(ShaderConfig* config) {
    initializeOpenGLFunctions();
	map<UniformName, const char*> uniformChars{
        std::make_pair(UniformName::ENTITY_COLOR, "entity_color"),
		std::make_pair(UniformName::POSITION, "position"),
		std::make_pair(UniformName::X_OFFSET, "x_offset"), //trzeba cos zrobic z tym syfem
		std::make_pair(UniformName::Y_OFFSET, "y_offset"),
		std::make_pair(UniformName::PROJECTION, "projection"),
		std::make_pair(UniformName::SAMPLER, "mySampler"),
        std::make_pair(UniformName::SCALE, "scale"),
        std::make_pair(UniformName::NORMAL, "normal"),
        std::make_pair(UniformName::MVP_MATRIX, "mvp_matrix")
	};

	ShaderCompiler shCompiler;

	try {
		program = shCompiler.compileShadersPair(config->getVertSrcPath(), config->getFragSrcPath());
	}
	catch (int a) {
		printf("Error no. %d during shader compilation", a);
	}

	std::list<UniformName>::const_iterator iterator;
	if (config->getMatricesList() != NULL) {
		for (iterator = config->getMatricesList()->begin(); iterator != config->getMatricesList()->end(); ++iterator) {
			const char* name = uniformChars[*iterator];
			GLint location = glGetUniformLocation(program, name);
			if (location < 0) {
				printf("glGetUniformLocation failed for value %s\n", name);
			}
            else {
				uniformMatrices[*iterator] = location;
			}
		}
	}
	if (config->getVectorsList() != NULL) {
		for (iterator = config->getVectorsList()->begin(); iterator != config->getVectorsList()->end(); ++iterator) {
			const char* name = uniformChars[*iterator];
			GLint location = glGetUniformLocation(program, name);
			if (location < 0) {
				printf("glGetUniformLocation failed for value %s\n", name);
			}
			else {
				uniformVectors[*iterator] = location;
			}
		}
	}
	if (config->getScalarsList() != NULL) {
		for (iterator = config->getScalarsList()->begin(); iterator != config->getScalarsList()->end(); ++iterator) {
			const char* name = uniformChars[*iterator];
			GLint location = glGetUniformLocation(program, name);
			if (location < 0) {
				printf("glGetUniformLocation failed for value %s\n", name);
			}
			else {
				uniformScalars[*iterator] = location;
			}
		}
	}
}

Shader::~Shader() {
	glUseProgram(0);
	printf("Warning!!! shader program was unbinded");
	glDeleteProgram(program);
}

void Shader::useShader() {
	glUseProgram(program);
}

bool Shader::setMatrice(UniformName matrixName, mat4 value) {
    GLuint uniformLocation = uniformMatrices.find(matrixName)->second;
	glUniformMatrix4fv(uniformLocation, 1, GL_TRUE, value_ptr(value));//dodac sprawdzanie czy w mapie mamy taka wartosc
	return true;
}

bool Shader::setVector(UniformName vectorName, vec4 value) {
	GLuint uniformLocation = uniformVectors.find(vectorName)->second;
	glUniform4fv(uniformLocation, 1, value_ptr(value));//dodac sprawdzanie czy w mapie mamy taka wartosc
	return true;
}

bool Shader::setScalar(UniformName scalarName, float value) {
	GLuint uniformLocation = uniformScalars.find(scalarName)->second;
	glUniform1f(uniformLocation, value); //dodac sprawdzanie czy w mapie mamy taka wartosc

	return true;
}

bool Shader::setScalar(UniformName scalarName, int value) {
	GLuint uniformLocation = uniformScalars.find(scalarName)->second;
	glUniform1i(uniformLocation, value); //dodac sprawdzanie czy w mapie mamy taka wartosc

	return true;
}


