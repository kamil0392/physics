#pragma once
#include <map>
#include "Shaderid.h"
#include "ShaderConfig.h"

using namespace std;

class ShaderConfigProvider {
public:
	ShaderConfigProvider();
	virtual ~ShaderConfigProvider();

	ShaderConfig* getConfig(ShaderId shaderId);
protected:
	map<ShaderId, ShaderConfig*> configs;
};