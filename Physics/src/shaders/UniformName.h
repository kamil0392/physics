#pragma once
enum class UniformName {
	X_OFFSET,
	Y_OFFSET,
    ENTITY_COLOR,
	POSITION,
	PROJECTION,
    MVP_MATRIX,
	SAMPLER,
    NORMAL,
	SCALE
};
