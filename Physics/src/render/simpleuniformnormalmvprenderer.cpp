#include "simpleuniformnormalmvprenderer.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <QDebug>
#include "collision/shape.h"
#include "helpers/algebra.h"

using glm::vec4;
using glm::mat4;

SimpleUniformNormalMvpRenderer::SimpleUniformNormalMvpRenderer(RenderProvider *renderProvider, Shader *shader){
    init(renderProvider, shader);
}

void SimpleUniformNormalMvpRenderer::render(Context *context, ComplexShape *complexShape){
    shader->useShader();
    shader->setVector(UniformName::ENTITY_COLOR, vec4(0.6f, 0.8f, 0.4f, 1.f));
    mat4 perspectiveCameraMatrix = context->getPerspectiveMatrix() * context->getCameraMatrix();
    VertexAttribBuffer* vertexBuffer;
    vec3 modelPosition, modelScale, modelRotation;

    for(auto shape : *(complexShape->getShapes())){
        modelRotation = shape->getRotation();
        modelPosition = shape->getOffset();
        modelScale = shape->getScalingFrame();

        shader->setVector(UniformName::NORMAL,
                          vec4(glm::rotate(
                                   glm::rotate(vec3(0,0,-1), modelRotation.y, vec3(1,0,0)),
                                   modelRotation.x,
                                   vec3(0,1,0)),
                              1.f));

        shader->setMatrice(UniformName::MVP_MATRIX,
                           perspectiveCameraMatrix *
                           getModelMatrix(modelPosition, modelRotation, modelScale, complexShape->getPosition(), complexShape->getRotationMatrix()));

        vertexBuffer = renderProvider->getVAB(shape->getPrimitiveId());

        vertexBuffer->bind();
        glDrawArrays(GL_TRIANGLES, 0, 3*vertexBuffer->getPrimitivesNumber());
        vertexBuffer->unbind();
    }
}

mat4 SimpleUniformNormalMvpRenderer::getModelMatrix(vec3 modelPosition, vec3 modelRotation, vec3 modelScale, vec3 complexShapePosition, mat4 complexShapeRotationMatrix)
{
    //complex
    mat4 result = mat4(1.f);
    result = glm::translate(result, complexShapePosition);
    result *= complexShapeRotationMatrix;

    //model
    result = glm::translate(result, modelPosition);

    mat4 shapeRotationMatrix = Algebra::createRotationMatrix(modelRotation);
    result *= shapeRotationMatrix;

    result = glm::scale(result, modelScale);
    return result;
}
