#pragma once

#include "abstractrenderer.h"
#include <map>
#include "shaders/ShaderId.h"
#include "simulation/context.h"
#include "VertexAttribBuffer.h"
#include "shaders/ShaderProvider.h"
#include "simulation/primitiveid.h"


class RenderProvider
{
public:
    RenderProvider();

    void init();

    void render(ShaderId shaderId, Context *context, ComplexShape *complexShape);
    VertexAttribBuffer *getVAB(PrimitiveId primitiveId);

protected:
    std::map<ShaderId, AbstractRenderer*> rendererMap;
    std::map<PrimitiveId, VertexAttribBuffer*> primitiveMap;
    ShaderProvider shaderProvider;
};
