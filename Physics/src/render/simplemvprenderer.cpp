#include "simplemvprenderer.h"
#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include <QDebug>
#include "helpers/algebra.h"
#include "collision/shape.h"

using glm::vec4;
using glm::mat4;

SimpleMVPRenderer::SimpleMVPRenderer(RenderProvider *renderProvider, Shader *shader){
    init(renderProvider, shader);
}

void SimpleMVPRenderer::render(Context *context, ComplexShape *complexShape){
    shader->useShader();
    shader->setVector(UniformName::ENTITY_COLOR, complexShape->getColor());
    mat4 perspectiveCameraMatrix = context->getPerspectiveMatrix() * context->getCameraMatrix();
    VertexAttribBuffer* vertexBuffer;
    list<Shape*>* shapes = complexShape->getShapes();
    vec3 modelPosition, modelScale, modelRotation, shapeRotation;

    for(std::list<Shape*>::iterator iterator = shapes->begin(), end = shapes->end(); iterator != end; iterator++){
        shapeRotation = (*iterator)->getRotation();
        modelPosition = (*iterator)->getOffset();
        modelRotation = shapeRotation;
        modelScale = (*iterator)->getScalingFrame();

        shader->setMatrice(UniformName::MVP_MATRIX,
                           perspectiveCameraMatrix *
                           getModelMatrix(modelPosition, modelRotation,
                                          modelScale,
                                          complexShape->getPosition(),
                                          complexShape->getRotationMatrix()));

        vertexBuffer = renderProvider->getVAB((*iterator)->getPrimitiveId());
        vertexBuffer->bind();
        glDrawArrays(GL_TRIANGLES, 0, 3*vertexBuffer->getPrimitivesNumber());
        vertexBuffer->unbind();
    }
}

mat4 SimpleMVPRenderer::getModelMatrix(vec3 modelPosition, vec3 modelRotation, vec3 modelScale, vec3 complexShapePosition, mat4 complexShapeRotationMatrix){
    //complex
    mat4 result = mat4();

    result = glm::translate(result, complexShapePosition);

    result *= complexShapeRotationMatrix;

    //model
    result = glm::translate(result, modelPosition);

    mat4 shapeRotationMatrix = Algebra::createRotationMatrix(modelRotation);
    result *= shapeRotationMatrix;

    result = glm::scale(result, modelScale);

    return result;
}
