#pragma once
#include <QOpenGLFunctions_4_0_Core>
#include <list>
#include <map>
#include <utility>

class VertexAttribBuffer: public QOpenGLFunctions_4_0_Core {
public:
	VertexAttribBuffer();
    void setData(float *data, int length, std::list<std::pair<int, int>> attribs);
	void breakedSetData(float *data, int length);
	void bind();
	void unbind();
	int getPrimitivesNumber();
	virtual ~VertexAttribBuffer();
protected:
	GLuint buffer;
	int primitivesNumber;
    int attribSum = 0; // sum of the .second of attribs
    std::list<std::pair<int, int>> attribs;
    std::map<std::pair<int, int>, int> attribPointerValues; // K : attrib V : (offset)
};
