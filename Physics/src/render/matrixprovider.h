#pragma once

#include "global.h"
#include "glm/glm.hpp"

using glm::vec2;
using glm::vec3;
using glm::mat4;

class TEST_COMMON_DLLSPEC MatrixProvider
{
public:
    MatrixProvider();

    void updateDimension(int w, int h);
    mat4 getPerspectiveMatrix() { return perspectiveMatrix;}


protected:
    mat4 perspectiveMatrix;
};
