#include "rendercomponent.h"
#include "glm/ext.hpp"
#include <stdio.h>
#include "input/inputhandler.h"
#include "simulation/simulationentity.h"
#include <qdebug.h>

RenderComponent::RenderComponent(ShaderId shaderId):
    shaderId(shaderId)
{
    color = vec4(0.5f, 0.4f, 0.8f, 0.5f);
}

void RenderComponent::update(){

}

void RenderComponent::render(Context *context){
/*
    context->getShaderProvider()->useShader(shaderId);

    context->getShaderProvider()->setVector(shaderId, UniformName::ENTITY_COLOR, color);

    context->getShaderProvider()->setMatrice(shaderId, UniformName::MVP_MATRIX,
                               context->getPerspectiveMatrix()*
                               context->getCameraMatrix()*
                               getModelMatrix(simulationEntity->getPosition(), simulationEntity->getRotation())
                               );

    VertexAttribBuffer* vertexBuffer = simulationEntity->getScene()->primitives[
            simulationEntity->getComplexShape()->getShapes()->front().getPrimitiveId()];
    vertexBuffer->bind();
    glDrawArrays(GL_TRIANGLES, 0, vertexBuffer->getPrimitivesNumber());
    vertexBuffer->unbind();*/
}

void RenderComponent::setSimulationEntity(SimulationEntity* simEntity){
    simulationEntity = simEntity;
}

glm::mat4 RenderComponent::getModelMatrix(glm::vec3 position, glm::vec2 rotation){
    glm::mat4 result = glm::translate(glm::mat4(), position);
    result = glm::rotate(result, rotation.x, glm::vec3(0.0f, 1.0f, 0.0f));
    result = glm::rotate(result, rotation.y, glm::vec3(1.0f, 0.0f, 0.0f));
    return result;
}
