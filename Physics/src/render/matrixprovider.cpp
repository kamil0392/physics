#include "matrixprovider.h"
#include <glm/gtc/matrix_transform.hpp>
#include "input/inputhandler.h"
#include <QKeyEvent>

MatrixProvider::MatrixProvider(){
    perspectiveMatrix = mat4();
}

void MatrixProvider::updateDimension(int w, int h){
    perspectiveMatrix = glm::perspective(45.f, float(w)/float(h), 0.1f, 100.f);
}

