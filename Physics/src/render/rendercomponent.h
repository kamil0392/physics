#pragma once

#include "shaders/ShaderProvider.h"
#include "shaders/ShaderId.h"
#include "render/VertexAttribBuffer.h"
#include "render/matrixprovider.h"
#include "simulation/camera.h"
#include "glm/glm.hpp"
#include "simulation/context.h"

class SimulationEntity;

class RenderComponent
{
public:
    RenderComponent(ShaderId shaderId);

    void update();
    void render(Context *context);

    void setColor(glm::vec4 color) { this->color = color; }
    void setSimulationEntity(SimulationEntity* simEntity);
    glm::mat4 getModelMatrix(glm::vec3 position, glm::vec2 rotation);

protected:
    SimulationEntity* simulationEntity;
    ShaderId shaderId;    
    glm::vec4 color;
};
