#include "renderprovider.h"
#include "helpers/objloader.h"
#include "simplemvprenderer.h"
#include "simpleuniformnormalmvprenderer.h"
#include "VertexBufferFactory.h"
#include <qdebug.h>

RenderProvider::RenderProvider(){
}

void RenderProvider::init(){
    shaderProvider.init();

    // Load primitives from obj
    ObjLoader objLoader;
    VertexAttribBuffer *buff;

    objLoader.load("..\\..\\Physics\\models\\cube.obj", 1.0f);
    buff = VertexBufferFactory::createPrimitive(&objLoader);
    primitiveMap[PrimitiveId::CUBE] = buff;

    objLoader.load("..\\..\\Physics\\models\\sphere.obj", 1.0f);
    buff = VertexBufferFactory::createPrimitive(&objLoader);
    primitiveMap[PrimitiveId::SPHERE] = buff;

    primitiveMap[PrimitiveId::PLANE] = VertexBufferFactory::createRectangle(vec2(100,100),vec3(-50,-50,0));

    rendererMap[ShaderId::SIMPLE_MVP_SHADER] = new SimpleMVPRenderer(this, shaderProvider.getShader(ShaderId::SIMPLE_MVP_SHADER));

    rendererMap[ShaderId::SIMPLE_SHADING_MVP_SHADER] = new SimpleMVPRenderer(this, shaderProvider.getShader(ShaderId::SIMPLE_SHADING_MVP_SHADER));

    rendererMap[ShaderId::UNIFORM_NORMAL_MVP] = new SimpleUniformNormalMvpRenderer(this, shaderProvider.getShader(ShaderId::UNIFORM_NORMAL_MVP));
}

void RenderProvider::render(ShaderId shaderId, Context *context, ComplexShape *complexShape){
    auto rendererIt = rendererMap.find(shaderId);
    if (rendererIt != rendererMap.end()){
        (*rendererIt).second->render(context, complexShape);
    } else {
        qDebug() << "renderer with id does not exist";
    }
}

VertexAttribBuffer *RenderProvider::getVAB(PrimitiveId primitiveId){
    return primitiveMap[primitiveId];
}
