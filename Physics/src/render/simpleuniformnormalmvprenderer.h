#pragma once

#include "abstractrenderer.h"
#include "glm/glm.hpp"
#include "simulation/context.h"

class SimpleUniformNormalMvpRenderer : public AbstractRenderer{
public:
    SimpleUniformNormalMvpRenderer(RenderProvider *renderProvider, Shader *shader);
    void render(Context *context, ComplexShape *complexShape);
    mat4 getModelMatrix(vec3 modelPosition, vec3 modelRotation, vec3 modelScale, vec3 complexShapePosition, glm::mat4 complexShapeRotationMatrix);
};
