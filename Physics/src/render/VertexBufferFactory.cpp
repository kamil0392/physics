#include "VertexBufferFactory.h"

VertexAttribBuffer *VertexBufferFactory::createPrimitive(ObjLoader *objloader)
{    
    VertexAttribBuffer* vertexAttribBuffer = new VertexAttribBuffer();

    std::list<std::pair<int, int>> attribs;
    attribs.push_back(std::make_pair(0, 4)); //position
    attribs.push_back(std::make_pair(1, 4)); //normals

    vertexAttribBuffer->setData(objloader->getVerticesAndNormals(), objloader->getVerticesAndNormalsSize(), attribs);

    return vertexAttribBuffer;
}

/*
 * deprecated - use createPrimitive instead
 */
VertexAttribBuffer * VertexBufferFactory::createRectangle(vec2 dimension, glm::vec3 offset)
{
    VertexAttribBuffer* vertexAttribBuffer = new VertexAttribBuffer();

    GLfloat data[] = { 0.0f + offset.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f,
        dimension.x + offset.x,  0.0f + offset.y, 0.0f + offset.z, 1.0f,
        dimension.x + offset.x, dimension.y + offset.y, 0.0f + offset.z, 1.0f,
        dimension.x + offset.x, dimension.y + offset.y, 0.0f + offset.z, 1.0f,
        0.f + offset.x,  dimension.y + offset.y, 0.0f + offset.z, 1.0f,
        0.f + offset.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f };
    std::list<std::pair<int, int>> attribs;
    attribs.push_front(std::make_pair(0, 4));

    vertexAttribBuffer->setData(data, 24, attribs);
    return vertexAttribBuffer;
}


VertexAttribBuffer * VertexBufferFactory::createGuiRectangle(vec2 dimension)
{
	VertexAttribBuffer* vertexAttribBuffer = new VertexAttribBuffer();

    /*GLfloat data[] = { 0.0f, dimension.y, 0.0f, 1.0f,
		dimension.x,  dimension.y, 0.0f, 1.0f,
		dimension.x, 0.0f, 0.0f, 1.0f,
		dimension.x, 0.0f, 0.0f, 1.0f,
		0.f,  0.0f, 0.0f, 1.0f,
        0.f, dimension.y, 0.0f, 1.0f };*/
    //vertexAttribBuffer->setData(data, 24, 4, 0);
	return vertexAttribBuffer;
}

/*
 * deprecated - use createPrimitive instead
 */
VertexAttribBuffer * VertexBufferFactory::createBox(vec3 dimension, glm::vec3 offset)
{
    VertexAttribBuffer* vertexAttribBuffer = new VertexAttribBuffer();

    dimension += offset;

    /*GLfloat data[] = { 0.0f + offset.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f,
        dimension.x,  0.0f + offset.y, 0.0f + offset.z, 1.0f,
        dimension.x, dimension.y, 0.0f + offset.z, 1.0f,
        dimension.x, dimension.y, 0.0f + offset.z, 1.0f,
        0.f + offset.x,  dimension.y, 0.0f + offset.z, 1.0f,
        0.f + offset.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f,

        0.0f + offset.x, 0.0f + offset.y, dimension.z, 1.0f,
        dimension.x,  0.0f + offset.y, dimension.z, 1.0f,
        dimension.x, dimension.y, dimension.z, 1.0f,
        dimension.x, dimension.y, dimension.z, 1.0f,
        0.f + offset.x,  dimension.y, dimension.z, 1.0f,
        0.f + offset.x, 0.0f + offset.y, dimension.z, 1.0f,

        0.0f + offset.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f,
        0.0f + offset.x, 0.0f + offset.y, dimension.z, 1.0f,
        0.0f + offset.x, dimension.y, dimension.z, 1.0f,
        0.0f + offset.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f,
        0.0f + offset.x, dimension.y, 0.0f + offset.z, 1.0f,
        0.0f + offset.x, dimension.y, dimension.z, 1.0f,

        dimension.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f,
        dimension.x, 0.0f + offset.y, dimension.z, 1.0f,
        dimension.x, dimension.y, dimension.z, 1.0f,
        dimension.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f,
        dimension.x, dimension.y, 0.0f + offset.z, 1.0f,
        dimension.x, dimension.y, dimension.z, 1.0f,

        0.0f + offset.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f,
        0.0f + offset.x, 0.0f + offset.y, dimension.z, 1.0f,
        dimension.x, 0.0f + offset.y, dimension.z, 1.0f,
        0.0f + offset.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f,
        dimension.x, 0.0f + offset.y, 0.0f + offset.z, 1.0f,
        dimension.x, 0.0f + offset.y, dimension.z, 1.0f,

        0.0f + offset.x, dimension.y, 0.0f + offset.z, 1.0f,
        0.0f + offset.x, dimension.y, dimension.z, 1.0f,
        dimension.x, dimension.y, dimension.z, 1.0f,
        0.0f + offset.x, dimension.y, 0.0f + offset.z, 1.0f,
        dimension.x, dimension.y, 0.0f + offset.z, 1.0f,
        dimension.x, dimension.y, dimension.z, 1.0f};*/

    //vertexAttribBuffer->setData(data, 144, 4, 0);
    return vertexAttribBuffer;
}
