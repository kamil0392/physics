#include "VertexAttribBuffer.h"
#include <qdebug>

VertexAttribBuffer::VertexAttribBuffer() : primitivesNumber(0) {
    initializeOpenGLFunctions();
	glGenBuffers(1, &buffer);
}

VertexAttribBuffer::~VertexAttribBuffer() {
	glDeleteBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VertexAttribBuffer::setData(float *data, int length, std::list<std::pair<int, int>> attribs) {
    this->attribs = attribs;

    for(auto attrib : attribs){
        attribPointerValues[attrib] = attribSum;
        attribSum += attrib.second;
    }

    primitivesNumber = length / (3*attribSum);

    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, length*sizeof(GLfloat), data, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VertexAttribBuffer::breakedSetData(float *data, int length) {
	primitivesNumber = length / 3;

	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, length*sizeof(GLfloat), data, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void VertexAttribBuffer::bind() {
    glBindBuffer(GL_ARRAY_BUFFER, buffer);

    for(auto attrib : attribs){
        glEnableVertexAttribArray(attrib.first);
        glVertexAttribPointer(attrib.first, attrib.second, GL_FLOAT, GL_FALSE, attribSum*sizeof(GLfloat), (void *) (attribPointerValues[attrib]*sizeof(GLfloat)));
    }
}

void VertexAttribBuffer::unbind() {
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

int VertexAttribBuffer::getPrimitivesNumber() {
	return primitivesNumber;
}
