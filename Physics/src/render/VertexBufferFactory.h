#pragma once
#include <QOpenGLFunctions_4_0_Core>
#include <list>

#include "VertexAttribBuffer.h"
#include "helpers/objloader.h"

#include <glm/glm.hpp>

using glm::vec2;
using glm::vec3;

class VertexBufferFactory: public QOpenGLFunctions_4_0_Core {
public:
	~VertexBufferFactory() {};

    static VertexAttribBuffer* createPrimitive(ObjLoader* objloader);
    static VertexAttribBuffer* createRectangle(vec2 dimension, vec3 offset);
	static VertexAttribBuffer* createGuiRectangle(vec2 dimension);

    static VertexAttribBuffer* createBox(vec3 dimension, vec3 offset);
private:
	VertexBufferFactory() {};
};
