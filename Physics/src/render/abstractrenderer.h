#pragma once

#include "shaders/Shader.h"
#include "collision/complexshape.h"

class RenderProvider;
class Context;

class AbstractRenderer
{
public:
    void init(RenderProvider *renderProvider, Shader *shader);
    virtual void render(Context *context, ComplexShape *complexShape) = 0;
protected:
    Shader *shader;
    RenderProvider *renderProvider;
};

