#pragma once

#include <glm/glm.hpp>

using glm::vec3;
using glm::mat3;
using glm::mat4;

class Algebra
{
public:
    Algebra();

    static mat4 createRotationMatrix(vec3 angles);
    static mat4 createRotationMatrix(vec3 baseVec, vec3 destVec);
    static mat4 createRotationMatrixFromRotationVector(vec3 vector);
    static vec3 getEulerAnglesFromRotationVector(vec3 vector);
    static vec3 getEulerAnglesFromRotationMatrix(mat4 matrix);
    static vec3 getEulerAnglesFromRotationMatrix(mat3 matrix);
};


