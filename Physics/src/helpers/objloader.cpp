#include "objloader.h"
#include <QFile>
#include <QTextStream>
#include <iostream>
#include <QDebug>
#include "glm/glm.hpp"

using glm::vec3;

void ObjLoader::load(const char *sourceFile, float scale)
{
    QFile file(sourceFile);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        vertices.clear();
        normals.clear();
        verticesAndNormals.clear();

        QTextStream textStream(&file);
        source.clear();
        while (!textStream.atEnd())
            source << textStream.readLine();
        file.close();

        parseItems(scale);
    }else{
        cout << "Could not open " << sourceFile << " " << endl;
    }
}

void ObjLoader::parseItems(float scale)
{
    QStringList sl, sl2;
    vector<GLfloat> tempVert;
    vector<GLfloat> tempNorm;

    for(int i=0; i<source.count(); i++){
        if(source[i].startsWith("v ")){ //verticle
            sl = source[i].mid(2).trimmed().split(" ");
            tempVert.push_back(sl[0].toFloat()*scale);
            tempVert.push_back(sl[1].toFloat()*scale);
            tempVert.push_back(sl[2].toFloat()*scale);
        }else if(source[i].startsWith("vn ")){ //normal
            sl = source[i].mid(2).trimmed().split(" ");
            tempNorm.push_back(sl[0].toFloat());
            tempNorm.push_back(sl[1].toFloat());
            tempNorm.push_back(sl[2].toFloat());
        }else if(source[i].startsWith("f ")){ //face
            sl = source[i].mid(2).trimmed().split(" ");
            vec3 verts[3];
            vec3 auxNormals[3];
            for(int j=0; j<3; j++){
                sl2 = sl[j].split("/");

                int vi = (sl2[0].toInt()-1)*3;
                int vni = (sl2[2].toInt()-1)*3;

                for(int k=0;k<3;k++){
                    verts[j][k] = tempVert[vi + k];
                    auxNormals[j][k] = tempNorm[vni+k];
                }

            }

            vec3 edge1 = verts[1] - verts[0];
            vec3 edge2 = verts[2] - verts[0];

            vec3 triangleNormal = glm::cross(edge1, edge2);

            vector<int> vertsOrder;

            vertsOrder.push_back(0);

            if (glm::dot(auxNormals[0], triangleNormal) > 0){
                vertsOrder.push_back(1);
                vertsOrder.push_back(2);
            } else {
                vertsOrder.push_back(2);
                vertsOrder.push_back(1);
            }

            for(auto it : vertsOrder){
                vertices.push_back(verts[it].x);
                vertices.push_back(verts[it].y);
                vertices.push_back(verts[it].z);
                vertices.push_back(1.0f);

                normals.push_back(auxNormals[it].x);
                normals.push_back(auxNormals[it].y);
                normals.push_back(auxNormals[it].z);
                normals.push_back(1.0f);
            }

        }

    }

    for(int i=0; i<vertices.size(); i += 4){
        for(int k=i; k<i+4; k++){
            verticesAndNormals.push_back(vertices[k]);
        }
        for(int k=i; k<i+4; k++){
            verticesAndNormals.push_back(normals[k]);
        }
    }

}
