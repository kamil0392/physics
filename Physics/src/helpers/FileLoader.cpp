#include "FileLoader.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;


FileLoader::FileLoader() {
}

FileLoader::~FileLoader() {
}

char* FileLoader::loadFile(const char *sourceFile) {
    ifstream file(sourceFile, ios_base::binary);

	char *src = 0;
	if (file.is_open())
	{
		file.seekg(0, ios_base::end);
        int fileSize = (int)file.tellg();
		file.seekg(0, ios_base::beg);
		src = new char[fileSize + 1];
		int iter = 0;
		while (file.get(src[iter])) {
			iter++;
		}
		src[iter] = 0;
		file.close();
	}
    else cout << "Unable to open file" << endl;
	return src;
}
