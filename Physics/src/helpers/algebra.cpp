 #include "algebra.h"
#include <glm/ext.hpp>

#define eps 0.001
using glm::vec2;

Algebra::Algebra(){
}

mat4 Algebra::createRotationMatrix(vec3 angles){
    mat4 result = mat4(1.f);

    result = glm::rotate(result, angles.z, vec3(0.0f, 0.0f, 1.0f));
    result = glm::rotate(result, angles.y, vec3(0.0f, 1.0f, 0.0f));
    result = glm::rotate(result, angles.x, vec3(1.0f, 0.0f, 0.0f));

    return result;
}

mat4 Algebra::createRotationMatrix(vec3 baseVec, vec3 destVec){ //using baseVec different than (0,0,-1) was not tested
    vec3 angles;

    if (glm::length(baseVec) < eps || glm::length(destVec) < eps){
        return mat4(1.f);
    }


    float xzBaseProjLen = glm::sqrt(glm::pow(baseVec.x, 2.f) + glm::pow(baseVec.z, 2.f));
    float xzDestProjLen = glm::sqrt(glm::pow(destVec.x, 2.f) + glm::pow(destVec.z, 2.f));

    if (xzBaseProjLen >= eps && xzDestProjLen >= eps){
        vec2 xzbaseProj = glm::normalize(vec2(baseVec.x, baseVec.z));
        vec2 xzdestProj = glm::normalize(vec2(destVec.x, destVec.z));
        float cosi = glm::dot(xzbaseProj, xzdestProj);
        if(cosi > 1.f){
            angles.y = 0;
        } else if(cosi<-1.f){
            angles.y = glm::pi<float>();
        } else{
            angles.y = glm::acos(cosi);
        }
        if (glm::cross(vec3(xzbaseProj, 0), vec3(xzdestProj, 0)).z > 0){
            angles.y *= -1;
        }
    } else {
        angles.y = 0.f;
    }

    vec2 xybaseProj = glm::normalize(vec2(1, 0));
    vec2 xydestProj = glm::normalize(vec2(xzDestProjLen, destVec.y));
    float cosi = glm::dot(xybaseProj, xydestProj);
    if(cosi > 1.f){
        angles.z = 0;
    } else if(cosi<-1.f){
        angles.z = glm::pi<float>();
    } else{
        angles.z = glm::acos(cosi);
    }
    if (glm::cross(vec3(xybaseProj, 0), vec3(xydestProj, 0)).z < 0){
        angles.z *= -1;
    }

    mat4 ret = mat4(1.f);

    ret = glm::rotate(ret, angles.y, vec3(0.0f, 1.0f, 0.0f));
    ret = glm::rotate(ret, angles.z, vec3(1.0f, 0.0f, 0.0f));
    return ret;
}




mat4 Algebra::createRotationMatrixFromRotationVector(vec3 vector){
    mat4 rotMat = mat4(1.f);
    if (glm::length(vector) < eps){
        return rotMat;
    }

    float xzProjLen = glm::sqrt(glm::pow(vector.x, 2.f) + glm::pow(vector.z, 2.f));
    float alpha = 0;
    if(xzProjLen >= eps){
        float cosi = vector.x / xzProjLen;
        if(cosi > 1.f){
            alpha = 0;
        } else if(cosi<-1.f){
            alpha = glm::pi<float>();
        } else{
            alpha = glm::acos(cosi);
        }
    }
    if(vector.z > 0){
        alpha *= -1.f;
    }

    vec3 aux = glm::rotateY(vec3(vector.x,0,0),alpha);

    float xyProjLen = glm::sqrt(glm::pow(vector.x, 2.f) + glm::pow(vector.y, 2.f));
    float beta = 0;
    if(xyProjLen >= eps){
        float l = glm::length(aux);
        float cosi = l/xyProjLen;
        if(cosi > 1.f){
            beta = 0;
        } else if(cosi<-1.f){
            beta = glm::pi<float>();
        } else{
            beta = glm::acos(cosi);
        }
    }
    if(vector.y < 0){
        beta *= -1.f;
    }

    rotMat = glm::rotate(rotMat, alpha, vec3(0,1,0));
    rotMat = glm::rotate(rotMat, beta, vec3(0,0,1));
    rotMat = glm::rotate(rotMat, glm::length(vector), vec3(1,0,0));
    rotMat = glm::rotate(rotMat, -beta, vec3(0,0,1));
    rotMat = glm::rotate(rotMat, -alpha, vec3(0,1,0));

    return rotMat;
}

vec3 Algebra::getEulerAnglesFromRotationVector(vec3 vector){
    mat4 rotMat = Algebra::createRotationMatrixFromRotationVector(vector);
    return getEulerAnglesFromRotationMatrix(rotMat);
}

vec3 Algebra::getEulerAnglesFromRotationMatrix(mat4 matrix){
    vec3 result;

    if(fabs(1-fabs(matrix[0].z)) > eps){
        result.y = -glm::asin(matrix[0].z);
        result.x = glm::atan(matrix[1].z / glm::cos(result.y),
                              matrix[2].z / glm::cos(result.y));

        result.z = glm::atan(matrix[0].y / glm::cos(result.y),
                              matrix[0].x / glm::cos(result.y));
    } else {
        result.z = 0;
        if (fabs(-1 - matrix[0].z) < eps){
            result.y = glm::pi<float>()/2.f;
            result.x = result.y+glm::atan(matrix[1].x, matrix[2].x);
        } else {
            result.y = -glm::pi<float>()/2.f;
            result.x = -result.y+glm::atan(-matrix[1].x, -matrix[2].x);
        }
    }

    return result;
}
vec3 Algebra::getEulerAnglesFromRotationMatrix(mat3 matrix){
    vec3 result;

    if(fabs(1-fabs(matrix[0].z)) > eps){
        result.y = -glm::asin(matrix[0].z);
        result.x = glm::atan(matrix[1].z / glm::cos(result.y),
                              matrix[2].z / glm::cos(result.y));

        result.z = glm::atan(matrix[0].y / glm::cos(result.y),
                              matrix[0].x / glm::cos(result.y));
    } else {
        result.z = 0;
        if (fabs(-1 - matrix[0].z) < eps){
            result.y = glm::pi<float>()/2.f;
            result.x = result.y+glm::atan(matrix[1].x, matrix[2].x);
        } else {
            result.y = -glm::pi<float>()/2.f;
            result.x = -result.y+glm::atan(-matrix[1].x, -matrix[2].x);
        }
    }

    return result;
}
