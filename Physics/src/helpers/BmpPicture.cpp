#include "BmpPicture.h"

BmpPicture::BmpPicture(int width, int height, unsigned char* data) {
	this->width = width;
	this->height = height;
	this->data = data;
}

BmpPicture::~BmpPicture() {
	delete data;
}

int BmpPicture::getWidth() {
	return width;
}
int BmpPicture::getHeight() {
	return height;
}
unsigned char* BmpPicture::getData() {
	return data;
}
