#include "BmpReader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <iostream>
#include <stddef.h>

using namespace std;

BmpReader::~BmpReader() {
}


BmpPicture* BmpReader::readBmpFile(const char* filePath) {
	ifstream file(filePath, ios_base::binary);
	unsigned char* data;
	int headerSize, width, height, pixSize = 0, picSize;

	if (file.is_open())
	{
		file.seekg(14, file.beg);
		file.read((char *)&headerSize, 4);
		file.read((char *)&width, 4);
		file.read((char *)&height, 4);
		file.seekg(2, file.cur);
		file.read((char *)&pixSize, 2);
		file.seekg(4, file.cur);
		file.read((char *)&picSize, 4);

		file.seekg(16, file.cur);
		data = new unsigned char[width*height * 3];
		file.read((char*)data, width*height * 3);

		file.close();

		return new BmpPicture(width, height, data);
	}
	else cout << "Unable to open file" << endl;
	return NULL;
}