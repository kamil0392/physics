#pragma once
class BmpPicture {
public:
	BmpPicture(int width, int height, unsigned char* data);
	virtual ~BmpPicture();

	int getWidth();
	int getHeight();
	unsigned char* getData();
protected:
	int width;
	int height;
	unsigned char* data;
};