#pragma once
class FileLoader {
public:
	FileLoader();
	virtual ~FileLoader();

	char* loadFile(const char *sourceFile);
};