#pragma once
#include "BmpPicture.h"

class BmpReader {
public:
	virtual ~BmpReader();

	static BmpPicture* readBmpFile(const char* filePath);
};