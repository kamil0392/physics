#pragma once
#include <QOpenGLFunctions_4_0_Core>
#include <QStringList>
#include <vector>

using namespace std;

class ObjLoader : public QOpenGLFunctions_4_0_Core{
public:
    ObjLoader() { }
    void load(const char *sourceFile, float scale);
    GLfloat *getVertices() { return &vertices[0]; }
    GLfloat *getNormals() { return &normals[0]; }
    GLfloat *getVerticesAndNormals() { return &verticesAndNormals[0]; }
    int getVerticesSize() { return vertices.size(); }
    int getVerticesAndNormalsSize() { return verticesAndNormals.size(); }
private:
    vector<GLfloat> vertices;
    vector<GLfloat> normals;
    vector<GLfloat> verticesAndNormals;
    QStringList source;
    void parseItems(float scale);
};
