#include "inputhandler.h"
#include <stdio.h>

InputHandler* InputHandler::instance = NULL;

InputHandler::InputHandler()
{

}

InputHandler* InputHandler::getInstance(){
    if (instance == NULL){
        printf("InputHandler used before initialization\n");
        exit(-1);
    }
    return instance;
}

void InputHandler::init(){
    instance = new InputHandler();
}

bool InputHandler::getKeyState(int key){
    if (keyState.find(key) != keyState.end()){
        return keyState[key];
    }
    return false;
}

void InputHandler::setKeyState(int key, bool state){
    keyState[key] = state;
}
