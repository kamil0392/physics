#pragma once

#include "global.h"
#include <map>

class TEST_COMMON_DLLSPEC InputHandler
{
public:
    static InputHandler* getInstance();
    static void init();

    bool getKeyState(int key);
    void setKeyState(int key, bool state);

private:
    InputHandler();
    static InputHandler* instance;

    std::map<int, bool> keyState;
};
