#include "physicscomponent.h"
#include "simulation/simulationentity.h"
#include "helpers/algebra.h"
#include <glm/ext.hpp>

PhysicsComponent::PhysicsComponent(float mass):
    mass(mass)
{
    linearVelocity = vec3(0,0,0);
    angularVelocity = vec3(0,0,0);
    force = vec3(0,0,0);
    torque = vec3(0,0,0);
    hasCollisionOccured = false;
}

void PhysicsComponent::setSimulationEntity(SimulationEntity* simulationEntity){
    this->simulationEntity = simulationEntity;
}

void PhysicsComponent::update(Context *context){
    simulationEntity->move(linearVelocity*context->getDelay());
    simulationEntity->rotate(Algebra::createRotationMatrixFromRotationVector(angularVelocity*context->getDelay()));

}

void PhysicsComponent::integrateImpulse(float delay){
    float mass = simulationEntity->getComplexShape()->getVolume();

    if (glm::length(impactImpulse) > 0.001 || glm::length(angularImpulse) > 0.001){
        linearVelocity += impactImpulse / mass;
        if (glm::length(linearVelocity)*delay < 0.001){
            linearVelocity = vec3(0,0,0);
        }
        angularVelocity += angularImpulse*glm::inverse(simulationEntity->getComplexShape()->getInertiaTensor());
        if (glm::length(angularImpulse)*delay < 0.001){
            angularImpulse = vec3(0,0,0);
        }
        impactImpulse = vec3(0,0,0);
        angularImpulse = vec3(0,0,0);
        lastFrameLinearVelocityChangeFromeForce = vec3(0,0,0);
    } else {
        lastFrameLinearVelocityChangeFromeForce = force * delay / mass;
        linearVelocity += lastFrameLinearVelocityChangeFromeForce;
        angularVelocity += (torque * delay) * glm::inverse(simulationEntity->getComplexShape()->getInertiaTensor());
    }
    hasCollisionOccured = false;
    force = vec3(0,0,0);
    torque = vec3(0,0,0);
}

void PhysicsComponent::applyImpulse(vec3 impulse, vec3 impulsePoint){
    impactImpulse += impulse;
    angularImpulse += glm::cross(impulsePoint, impulse);
    integrateImpulse(0.02f);
}

void PhysicsComponent::applyForce(vec3 force, vec3 forcePoint){
    this->force += force;
    torque += glm::cross(forcePoint, force);
    integrateImpulse(0.02f);
}
