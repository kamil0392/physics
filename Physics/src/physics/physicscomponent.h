#pragma once

#include "global.h"
#include "simulation/simulationentity.h"
#include "simulation/context.h"
#include "glm/glm.hpp"
#include <qdebug.h>

using glm::vec3;

class TEST_COMMON_DLLSPEC PhysicsComponent
{
public:
    PhysicsComponent() {}
    PhysicsComponent(float mass);
    void setSimulationEntity(SimulationEntity* simulationEntity);
    void setVelocity(vec3 vel) { linearVelocity = vel; }
    vec3 getVelocity() { return linearVelocity; }
    void update(Context *context);
    virtual vec3 getLinearVelocity() { return linearVelocity; }
    virtual vec3 getLastFrameAcc() { return lastFrameLinearVelocityChangeFromeForce; }
    virtual vec3 getAngularVelocity() { return angularVelocity; }
    virtual void applyImpulse(vec3 impulse, vec3 impulsePoint);
    virtual void applyForce(vec3 force, vec3 forcePoint);
    virtual vec3 getCurrentForce() { return force; }
    void setCollision() { hasCollisionOccured = true; }
private:
    void integrateImpulse(float delay);

protected:
    SimulationEntity *simulationEntity;
    vec3 linearVelocity;
    vec3 lastFrameLinearVelocityChangeFromeForce;
    vec3 angularVelocity;
    vec3 impactImpulse;
    vec3 angularImpulse;
    vec3 collisionNormal;
    vec3 force;
    vec3 torque;
    float mass;
    bool hasCollisionOccured;
};

