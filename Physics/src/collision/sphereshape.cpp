#include "sphereshape.h"
#include "collision/complexshape.h"

SphereShape::SphereShape(vec3 offset, vec3 rotation, float scale)
    : Shape(PrimitiveId::SPHERE, offset, rotation, vec3(scale, scale, scale)){
    inertiaTensor = mat3(1.f);
    inertiaTensor *= 2.f/5.f*getVolume()*scalingFrame.x*scalingFrame.x;
}

float SphereShape::getVolume(){
    return 4.f/3.f*glm::pi<float>()*std::powf(scalingFrame.x, 3.f);
}
