#pragma once

#include <glm/glm.hpp>
#include "simulation/primitiveid.h"

using glm::vec3;
using glm::mat3;
class ComplexShape;

class Shape
{
public:
    Shape() {}
    Shape(PrimitiveId primitiveId, vec3 offset, vec3 rotation, vec3 scalingFrame);
    PrimitiveId getPrimitiveId() { return primitiveId; }
    virtual vec3 getOffset() { return offset; }
    vec3 getScalingFrame() { return scalingFrame; }
    void setScalingFrame(vec3 newScalingFrame) { scalingFrame = newScalingFrame; }
    vec3 getRotation() { return rotation; }
    void setOffset(vec3 newOffset) { offset = newOffset; }
    void move(vec3 offset) { this->offset += offset; }
    virtual float getVolume() { return 0; }
    mat3 getInertiaTensor() { return inertiaTensor; }
protected:
    PrimitiveId primitiveId;
    vec3 offset;
    vec3 scalingFrame;
    vec3 rotation;
    mat3 inertiaTensor;
};
