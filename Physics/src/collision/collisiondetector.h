#pragma once

#include "collision/complexShape.h"
#include "simulation/simulationentity.h"
#include "collision/planeshape.h"
#include "collision/cubeshape.h"
#include "collision/sphereshape.h"
#include "collision/collisioninfo.h"

typedef bool(*CollisionPointer)(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2);

class Edge {
public:
    Edge(vec3 point1, vec3 point2, vec3 normal):
        point1(point1), point2(point2), normal(glm::normalize(normal)){}
    vec3 point1;
    vec3 point2;
    vec3 normal;
};

class CollisionDetector
{
public:
    CollisionDetector();

    static void detectCollision(SimulationEntity *simulationEntity1, SimulationEntity *simulationEntity2);

    static bool detectSphereSphereCollision(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2);
    static bool detectSpherePlaneCollision(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2);
    static bool detectSphereCubeCollision(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2);
    static bool detectCubePlaneCollision(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2);
    static bool detectCubeCubeCollision(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2);
    static bool detectCubeCubeCollision2(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2);

    // broad phase colliders
    static bool detectSphereCellCollision(vec3 spherePos, float radius, std::tuple<float, float, float> cellPos, float maxRadius);
    static bool detectPlaneCellCollision(vec3 planePos, vec3 planeNormal, std::tuple<float, float, float> cellPos, float maxRadius);

    static vector<vec3> getGlobalFrameVertices(vec3 centre, vec3 scale, vec3 xBase, vec3 yBase, vec3 zBase);

    static CollisionPointer collisions[3][3];
protected:
    static CollisionInfo checkOBBVertsAndAABB(mat3 aabbFullRotMat, vec3 aabb, glm::mat3 gFToObbShapeLFRotMat, vec3 obbPos, vec3 obb[3], Shape *obbShape);
    static CollisionInfo checkOBBsVerts(mat3 s1FullRotMat, mat3 s2FullRotMat, Shape *shape1, Shape *shape2, mat3 gFToS1LFRotMat, mat3 gFToS2LFRotMat,
                                        vec3 s1PosInS2LF, vec3 s2PosInS1LF, vec3 s1NormalsInS2LF[3], vec3 s2NormalsInS1LF[3]);
    static CollisionInfo checkEdgeAndAABB(mat3 aabbFullRotMat, vec3 aabb, mat3 gFToObbShapeLFRotMat, vec3 obbPos, vec3 obb[3]);
    static CollisionInfo checkEdgeEdge(vec3 p1, vec3 dir1, vec3 p2, vec3 dir2, mat3 aabbFullRotMat, mat3 gFToObbShapeLFRotMat, vec3 obbPos);
    static pair<vec3, vec3> getClosestPointsOfEdges(vec3 p1, vec3 dir1, vec3 p2, vec3 dir2);
    static bool isEdgeInAABB(Edge edge, vec3 AABB);
};
