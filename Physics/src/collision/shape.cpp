#include "shape.h"

Shape::Shape(PrimitiveId primitiveId, vec3 offset, vec3 rotation, vec3 scalingFrame):
    primitiveId(primitiveId), offset(offset), scalingFrame(scalingFrame){
    this->rotation = vec3(glm::radians(rotation.x), glm::radians(rotation.y), glm::radians(rotation.z));
}
