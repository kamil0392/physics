#pragma once

#include "glm/glm.hpp"

using glm::vec3;

class CollisionInfo
{
public:
    CollisionInfo();
    CollisionInfo(vec3 s1ColPointInS1LF, vec3 s2ColPointInS2LF, vec3 collisionNormalInGF, float interpenetrationDepth);

    bool operator <(const CollisionInfo& secondInfo);
    bool operator >(const CollisionInfo& secondInfo);

    vec3 s1ColPointInS1LF;
    vec3 s2ColPointInS2LF;
    vec3 collisionNormalInGF;
    float interpenetrationDepth;
    bool wasCollisionFound = false;
};

