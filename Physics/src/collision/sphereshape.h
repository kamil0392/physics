#pragma once

#include "collision/shape.h"
#include <QDebug>
#include "glm/gtc/constants.hpp"
#include <cmath>

class ComplexShape;

class SphereShape : public Shape
{
public:
    SphereShape(glm::vec3 offset, glm::vec3 rotation, float scale);
    float getVolume();
};
