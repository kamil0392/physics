#pragma once

#include "simulation/simulationentity.h"
#include "glm/glm.hpp"

using glm::vec3;

class InterpenetrationResolver
{
public:
    InterpenetrationResolver();

    static void linearResolution(vec3 collisionNormal, float interpenetrationDepth, SimulationEntity *entity1, SimulationEntity *entity2);
};
