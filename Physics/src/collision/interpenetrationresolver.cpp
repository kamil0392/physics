#include "interpenetrationresolver.h"

InterpenetrationResolver::InterpenetrationResolver(){

}

void InterpenetrationResolver::linearResolution(vec3 collisionNormal, float interpenetrationDepth, SimulationEntity *entity1, SimulationEntity *entity2){
    if (entity1->isMovable() && entity2->isMovable()){
        float summarizedMass = entity1->getComplexShape()->getVolume() + entity2->getComplexShape()->getVolume();
        entity1->move(collisionNormal*interpenetrationDepth*entity2->getComplexShape()->getVolume()/summarizedMass);
        entity2->move(-collisionNormal*interpenetrationDepth*entity1->getComplexShape()->getVolume()/summarizedMass);
    } else if (entity1->isMovable()){
        entity1->move(collisionNormal*interpenetrationDepth);
    } else if (entity2->isMovable()){
        entity2->move(-collisionNormal*interpenetrationDepth);
    }
}
