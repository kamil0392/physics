#include "collisionresolver.h"
#include <glm/gtx/perpendicular.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <qdebug.h>
#include "helpers/algebra.h"

using glm::mat3;

CollisionResolver::CollisionResolver(){
}

void CollisionResolver::resolveMovableMovable(glm::vec3 collisionNormal, vec3 relativeVelocity, SimulationEntity *entity1, glm::vec3 collisionPoint1, SimulationEntity *entity2, glm::vec3 collisionPoint2, float e){
    float impulse = (e*(glm::dot(entity1->getPhysicsComponent()->getLastFrameAcc(), collisionNormal) -
                     glm::dot(entity2->getPhysicsComponent()->getLastFrameAcc(), collisionNormal))
                -(e+1)*glm::dot(relativeVelocity, collisionNormal)) /
            (1/entity1->getComplexShape()->getVolume() + 1/entity2->getComplexShape()->getVolume()
             + glm::dot(collisionNormal,
                        glm::cross(glm::inverse(entity1->getComplexShape()->getInertiaTensor())*glm::cross(collisionPoint1, collisionNormal),
                                   collisionPoint1))
             + glm::dot(collisionNormal,
                        glm::cross(glm::inverse(entity2->getComplexShape()->getInertiaTensor())*glm::cross(collisionPoint2, collisionNormal),
                                   collisionPoint2)));

    vec3 tangentialVel = -(relativeVelocity - glm::dot(relativeVelocity, collisionNormal)*collisionNormal);
    vec3 frictionImpulse;
    if (glm::length(tangentialVel) > 0.001){
        float frictionCoefficent = entity1->getComplexShape()->getFrictionCoefficent() * entity2->getComplexShape()->getFrictionCoefficent();
        frictionImpulse = impulse*frictionCoefficent*glm::normalize(tangentialVel);
        vec3 currentImpulse = glm::dot(relativeVelocity, tangentialVel)*tangentialVel*(entity1->getComplexShape()->getVolume()+entity2->getComplexShape()->getVolume());
        if (glm::length(currentImpulse) < glm::length(frictionImpulse)){
            frictionImpulse = -currentImpulse;
        }
    }

    entity1->getPhysicsComponent()->applyImpulse(impulse*collisionNormal + frictionImpulse, collisionPoint1);
    entity2->getPhysicsComponent()->applyImpulse(-impulse*collisionNormal - frictionImpulse, collisionPoint2);
}

void CollisionResolver::resolveMovableStationary(glm::vec3 collisionNormal, vec3 relativeVelocity, SimulationEntity *movable, glm::vec3 collisionPointOnMovable, SimulationEntity *stationary){
    float e = 0.7f; //coefficient of restitution

    float impulse = (e*glm::dot(movable->getPhysicsComponent()->getLastFrameAcc(), collisionNormal) -
                    (e+1)*glm::dot(relativeVelocity, collisionNormal)) /
                    (1/movable->getComplexShape()->getVolume() + glm::dot(collisionNormal,
                                                                          glm::cross(glm::inverse(movable->getComplexShape()->getInertiaTensor())*glm::cross(collisionPointOnMovable, collisionNormal),
                                                                                     collisionPointOnMovable)));

    vec3 tangentialVel = -(relativeVelocity - glm::dot(relativeVelocity, collisionNormal)*collisionNormal);
    vec3 frictionImpulse;
    if (glm::length(tangentialVel) > 0.001){
        tangentialVel = glm::normalize(tangentialVel);
        float frictionCoefficent = movable->getComplexShape()->getFrictionCoefficent() * stationary->getComplexShape()->getFrictionCoefficent();
        frictionImpulse = impulse*frictionCoefficent*tangentialVel;
        vec3 currentImpulse = glm::dot(relativeVelocity, tangentialVel)*tangentialVel*movable->getComplexShape()->getVolume();
        if (glm::length(currentImpulse) < glm::length(frictionImpulse)){
            frictionImpulse = -currentImpulse;
        }
    }

    movable->getPhysicsComponent()->applyImpulse(impulse*collisionNormal + frictionImpulse, collisionPointOnMovable);
}
