#include "complexshape.h"
#include "glm/ext.hpp"
#include <string>
#include "collision/shape.h"
#include "helpers/algebra.h"

ComplexShape::~ComplexShape(){
    for(auto shape : shapes){
        delete shape;
    }
}

ComplexShape::ComplexShape(list<Shape*> shapes, glm::vec3 position, glm::vec3 rotation, vec4 color, float frictionCoefficent):
    shapes(shapes), position(position), color(color), frictionCoefficent(frictionCoefficent){
    this->rotation = vec3(glm::radians(rotation.x), glm::radians(rotation.y), glm::radians(rotation.z));
    rotationMatrix = Algebra::createRotationMatrix(this->rotation);
    moveCenterMassToOrigin();
    recalcVolume();
    recalcInertiaTensor();
}

void ComplexShape::rescale(float scaleFactor){
}


void ComplexShape::rotate(mat4 rotationMatrix){
    this->rotationMatrix = rotationMatrix*this->rotationMatrix;
    rotation = Algebra::getEulerAnglesFromRotationMatrix(this->rotationMatrix);
}

void ComplexShape::moveCenterMassToOrigin(){
    vec3 centerOfMass;
    float currentMass = 0.f;

    for(auto shape : shapes){
        float density = 1; //in future get this value from material
        float shapeVolume = shape->getVolume();
        if (shapeVolume > 0.f){
            centerOfMass += shape->getOffset()*density*shapeVolume;
            currentMass += density*shapeVolume;
        }
    }
    if (currentMass > 0.f)
        centerOfMass /= currentMass;

    position += centerOfMass;
    for(auto shape : shapes){
        shape->move(-centerOfMass);
    }
}

void ComplexShape::recalcVolume(){
    volume = 0;
    for(auto shape : shapes){
        volume += shape->getVolume();
    }
}

void ComplexShape::recalcInertiaTensor(){
    inertiaTensor = mat3(0.f);

    for(auto shape : shapes){
        mat3 rotationMat = mat3(Algebra::createRotationMatrix(shape->getRotation()));
        mat3 transformedInertiaTensor = rotationMat * shape->getInertiaTensor() * glm::transpose(rotationMat);
        vec3 r = shape->getOffset();
        mat3 translationAddition;
        translationAddition[0] = -shape->getVolume() * r * r.x;
        translationAddition[0].x += shape->getVolume()*(r.x*r.x+r.y*r.y+r.z*r.z);
        translationAddition[1] = -shape->getVolume() * r * r.y;
        translationAddition[1].y += shape->getVolume()*(r.x*r.x+r.y*r.y+r.z*r.z);
        translationAddition[2] = -shape->getVolume() * r * r.z;
        translationAddition[2].z += shape->getVolume()*(r.x*r.x+r.y*r.y+r.z*r.z);

        inertiaTensor += transformedInertiaTensor + translationAddition;
    }
}


void ComplexShape::setRotation(vec3 vector){
    this->rotationMatrix = Algebra::createRotationMatrix(vec3(0,0,-1), vector);
    rotation = Algebra::getEulerAnglesFromRotationMatrix(this->rotationMatrix);
}
