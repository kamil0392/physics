#pragma once

#include <list>
#include "glm/glm.hpp"
#include <QDebug>
#include "global.h"

using std::list;
using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;

class Shape;

class TEST_COMMON_DLLSPEC ComplexShape
{
public:
    ComplexShape(){}
    ~ComplexShape();
    ComplexShape(list<Shape*> shapes, vec3 position, vec3 rotation, vec4 color=vec4(0.9f, 0.2f, 0.4f, 1.f), float frictionCoefficent = 0.f);

    vec3 getPosition() { return position; }
    void move(vec3 offset) { position += offset; }
    void setPosition(vec3 newPosition) { position = newPosition; }
    vec3 getRotation() { return rotation; }
    void setRotation(vec3 vector);
    mat4 getRotationMatrix() { return rotationMatrix; }
    void setRotationMatrix(mat4 rotationMatrix) { this->rotationMatrix = rotationMatrix; }
    void rotate(glm::mat4 rotationMatrix);
    void rescale(float scaleFactor);
    list<Shape*>* getShapes() { return &shapes; }
    float getVolume() { return volume; }
    virtual mat3 getInertiaTensor() { return inertiaTensor; }
    vec4 getColor() { return color; }
    void setColor(vec4 color) { this->color = color; }
    float getFrictionCoefficent() { return frictionCoefficent; }
protected:
    void moveCenterMassToOrigin();
    void recalcVolume();
    void recalcInertiaTensor();
    list<Shape*> shapes;
    vec3 position;
    vec3 rotation;
    mat4 rotationMatrix;
    float volume;
    mat3 inertiaTensor;

    vec4 color; //TODO: move to material component
    float frictionCoefficent; //TODO: move to material component
};

