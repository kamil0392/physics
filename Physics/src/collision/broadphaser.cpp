#include <limits>
#include "broadphaser.h"
#include "complexshape.h"
#include "QDebug"
#include "simulation/simulationentity.h"
#include "collision/shape.h"
#include "collision/sphereshape.h"
#include "collision/collisiondetector.h"
#include "helpers/algebra.h"
#include "glm/ext.hpp"

//#define DEBUG_CUBE
//#define DEBUG_COMPLEX

using glm::mat3;

// http://http.developer.nvidia.com/GPUGems3/gpugems3_ch32.html

// returns position in global frame
vec3 BroadPhaser::getComplexAveragePosition(ComplexShape* complex){
    vec3 averagePosition;
    mat3 rotation = mat3(complex->getRotationMatrix());

    for(auto shape : (*complex->getShapes()))
        averagePosition += complex->getPosition() + rotation * shape->getOffset();

    averagePosition /= complex->getShapes()->size();

    return averagePosition;
}

void BroadPhaser::init(map<int, SimulationEntity*> entityMap, Scene* scene){
    regularEntities.clear();
    hugeEntities.clear();
    planes.clear();
    debugEntityId = -1;
    maxRadius = 0;
    float averageRadius = 0;
    this->scene = scene;

#ifdef DEBUG_COMPLEX
    list<Shape*> shapeList;
    SimulationEntity *simulationEntity;
#endif

    for (auto entity : entityMap){
        SimulationEntity* simulationEntity = entity.second;
        ComplexShape* complex = simulationEntity->getComplexShape();
        vec3 centre = complex->getPosition();//getComplexAveragePosition(complex);
        float radius = 0.f;
        Shape* firstShape = (*complex->getShapes()->begin());
        if(firstShape->getPrimitiveId() == PrimitiveId::PLANE){
            vec3 planeRotGF = simulationEntity->getRotation() + (*simulationEntity->getComplexShape()->getShapes()->begin())->getRotation();
            vec3 planeNormal = glm::rotate(glm::rotate(glm::rotate(vec3(0,0,-1), planeRotGF.x, vec3(1,0,0)), planeRotGF.y, vec3(0,1,0)), planeRotGF.z, vec3(0,0,1));
            planes[entity.first] = std::make_pair(simulationEntity, planeNormal);
        }else if(simulationEntity->isMovable()){
            for (Shape* shape : *(complex->getShapes())){
                switch( shape->getPrimitiveId() ){
                case PrimitiveId::SPHERE:
                {
                    mat3 shapeRotMat(simulationEntity->getRotationMatrix());
                    vec3 shapePosInGlobalFrame = simulationEntity->getPosition() + shapeRotMat * shape->getOffset();
                    radius = std::max(radius, glm::length(shapePosInGlobalFrame - centre) + shape->getScalingFrame().x);
                    break;
                }
                case PrimitiveId::CUBE:
                {
                    mat3 shapeRotMat(simulationEntity->getRotationMatrix());
                    vec3 shapePosInGlobalFrame = simulationEntity->getPosition() + shapeRotMat * shape->getOffset();
                    vec3 cubeScale = shape->getScalingFrame();
                    vec3 u[3] = { vec3(1.0f, 0, 0), vec3(0, 1.0f, 0), vec3(0, 0, 1.0f) };
                    mat3 rotationMatrix = shapeRotMat * mat3(Algebra::createRotationMatrix(shape->getRotation()));
                    for(int i=0; i<3; i++){
                        u[i] = rotationMatrix * u[i];
                    }
                    for(vec3 point : CollisionDetector::getGlobalFrameVertices(shapePosInGlobalFrame, cubeScale, u[0], u[1], u[2])){
                        radius = std::max(radius, glm::distance(centre, point));
#ifdef DEBUG_COMPLEX
                        shapeList.push_back(new SphereShape(point, vec3(0,0,0), 0.3f));
#endif
                    }

                    break;
                }
                }
            }
            regularEntities[entity.first] = std::make_pair(entity.second, radius);
            averageRadius += radius;
            //maxRadius = std::max(radius, maxRadius);
#ifdef DEBUG_COMPLEX
            shapeList.push_back(new SphereShape(centre, vec3(0,0,0), radius));
        }
    }
    simulationEntity = new SimulationEntity(
                scene, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                ComplexShape(shapeList, vec3(0.f,0.f,0.f), vec3(0,0,0), vec4(1,0,1,0.3)),
                new PhysicsComponent(1.f));
    simulationEntity->setCollidability(false);
    scene->addEntity(simulationEntity);
#else
        }
    }
#endif
    averageRadius /= regularEntities.size();

    for(auto it = regularEntities.begin(); it != regularEntities.end(); ){
        auto entry = *it;
        float radius = entry.second.second;
        if(radius > 2*averageRadius){ //magic number
            hugeEntities[entry.first] = entry.second;
            regularEntities.erase(it++);
        }else{
            maxRadius = std::max(radius, maxRadius);
            ++it;
        }
    }
}

vector<std::tuple<float, float, float> > BroadPhaser::getCollidingCells(vec3 pos, float radius){
    vector<std::tuple<float, float, float>> result;
    auto centreCell = std::make_tuple(
                2*maxRadius*(floor((pos.x + maxRadius) / (2*maxRadius))),
                2*maxRadius*(floor((pos.y + maxRadius) / (2*maxRadius))),
                2*maxRadius*(floor((pos.z + maxRadius) / (2*maxRadius))));
    for(int x=-1; x<2; x+=1){
        for(int y=-1; y<2; y+=1){
            for(int z=-1; z<2; z+=1){
                auto cellId = std::make_tuple(
                            std::get<0>(centreCell)+2*maxRadius*x,
                            std::get<1>(centreCell)+2*maxRadius*y,
                            std::get<2>(centreCell)+2*maxRadius*z);

                if (CollisionDetector::detectSphereCellCollision(pos, radius, cellId, maxRadius)){
                    result.push_back(cellId);
                }
            }
        }
    }
    return result;
}

set<set<SimulationEntity*>> BroadPhaser::broadPhase(){
    set<set<SimulationEntity*>> result;
    //K - tuple with centre of cell
    //V - list of simulation entities which collides with particular cell
    map<std::tuple<float, float, float>, vector<pair<int, SimulationEntity*>>> cellEntityMap;

#ifdef DEBUG_CUBE
    map<std::tuple<float, float, float>, Shape*> shapes;
    list<Shape*> shapeList;
#endif

    for(auto entity : regularEntities){
        auto value = entity.second;
        vec3 pos = value.first->getPosition();
        float radius = value.second;
        for(auto key : getCollidingCells(pos, radius)){
            if (cellEntityMap.find(key) == cellEntityMap.end()){
                vector<pair<int, SimulationEntity*>> v = {std::make_pair(entity.first, value.first)};
                cellEntityMap[key] = v;
            }else{
                cellEntityMap[key].push_back(std::make_pair(entity.first, value.first));
            }
#ifndef DEBUG_CUBE
        }
    }
#else DEBUG_CUBE
            shapes[key] = new CubeShape(vec3(std::get<0>(key), std::get<1>(key), std::get<2>(key)), vec3(0,0,0), vec3(1,1,1)*maxRadius);
        }
        shapeList.push_back(new SphereShape(pos, vec3(0,0,0), radius));
    }
    if(shapeList.size() > 0){
        for(auto shape: shapes)
            shapeList.push_back(shape.second);

        SimulationEntity* simulationEntity = new SimulationEntity(
                    scene, ShaderId::SIMPLE_SHADING_MVP_SHADER,
                    ComplexShape(shapeList, vec3(0.f,0.f,0.f), vec3(0,0,0), vec4(1,1,0,0.3)),
                    new PhysicsComponent(1.f));
        simulationEntity->setCollidability(false);
        debugEntityId = scene->addEntity(simulationEntity);
    }
#endif

    //for every "abnormal" object (planes, huge entities) check
    //if it collides with cell, if so, add it to the cell
    for(auto cell : cellEntityMap){
        for(auto plane : planes){
            auto planeValue = plane.second;
            if(CollisionDetector::detectPlaneCellCollision(planeValue.first->getPosition(), planeValue.second, cell.first, maxRadius)){
                cellEntityMap[cell.first].push_back(std::make_pair(plane.first, planeValue.first));
            }
        }

        for(auto hugeEntity : hugeEntities){
            auto eVal = hugeEntity.second;
            if(CollisionDetector::detectSphereCellCollision(eVal.first->getPosition(), eVal.second, cell.first, maxRadius)){
                cellEntityMap[cell.first].push_back(std::make_pair(hugeEntity.first, eVal.first));
            }
        }
    }

    for(auto entry : cellEntityMap){
        auto cell = entry.second;
        for(auto e1It = cell.begin(); e1It != cell.end(); e1It++){
            for(auto e2It = std::next(e1It); e2It != cell.end(); e2It++){
                int e1Id = (*e1It).first;
                int e2Id = (*e2It).first;
                if(planes.find(e1Id) == planes.end() && planes.find(e2Id) == planes.end()){
                    pair<SimulationEntity*, float> e1,e2;
                    //this definitely could be more elegant but meh
                    if(regularEntities.find(e1Id) != regularEntities.end())
                       e1 = regularEntities[e1Id];
                    else
                       e1 = hugeEntities[e1Id];

                    if(regularEntities.find(e2Id) != regularEntities.end())
                       e2 = regularEntities[e2Id];
                    else
                       e2 = hugeEntities[e2Id];

                    if(glm::length(e1.first->getPosition() - e2.first->getPosition() ) > e1.second + e2.second )
                        continue;
                }
                set<SimulationEntity*> pair;
                pair.insert((*e1It).second);
                pair.insert((*e2It).second);
                result.insert(pair);
            }
        }
    }

    return result;
}
