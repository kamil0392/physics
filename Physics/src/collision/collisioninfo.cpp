#include "collisioninfo.h"

CollisionInfo::CollisionInfo(){
}


CollisionInfo::CollisionInfo(vec3 s1ColPointInS1LF, glm::vec3 s2ColPointInS2LF, vec3 collisionNormalInGF, float interpenetrationDepth):
    s1ColPointInS1LF(s1ColPointInS1LF), s2ColPointInS2LF(s2ColPointInS2LF), collisionNormalInGF(collisionNormalInGF), interpenetrationDepth(interpenetrationDepth){
    wasCollisionFound = true;
}

bool CollisionInfo::operator <(const CollisionInfo& secondInfo){
    if (!wasCollisionFound && !secondInfo.wasCollisionFound)
        return true;

    if (!wasCollisionFound)
        return false;

    if (!secondInfo.wasCollisionFound)
        return true;

    return interpenetrationDepth < secondInfo.interpenetrationDepth;
}

bool CollisionInfo::operator >(const CollisionInfo& secondInfo){
    return ! ((*this) < secondInfo);
}
