#pragma once

#include "collision/shape.h"

class PlaneShape : public Shape
{
public:
    PlaneShape(vec3 offset, vec3 rotation, vec3 scalingFrame);
    float getVolume() {return 0.f;}
};

