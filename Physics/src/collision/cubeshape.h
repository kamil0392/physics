#pragma once

#include "collision/shape.h"
#include <QDebug>
#include <cmath>

class ComplexShape;

class CubeShape : public Shape
{
public:
    CubeShape(glm::vec3 offset, glm::vec3 rotation, glm::vec3 scalingFrame);
    float getVolume();
};
