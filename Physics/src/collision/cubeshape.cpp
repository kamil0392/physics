#include "cubeshape.h"
#include "glm/ext.hpp"
#include "collision/complexshape.h"

CubeShape::CubeShape(vec3 offset, vec3 rotation, vec3 scalingFrame)
    : Shape(PrimitiveId::CUBE, offset, rotation, scalingFrame){
    inertiaTensor = mat3(1.f);
    inertiaTensor *= 1/12.f * getVolume() * vec3(
                (glm::pow2(scalingFrame.y) + glm::pow2(scalingFrame.z)),
                (glm::pow2(scalingFrame.x) + glm::pow2(scalingFrame.z)),
                (glm::pow2(scalingFrame.x) + glm::pow2(scalingFrame.y)));
}

float CubeShape::getVolume(){
    return 8*scalingFrame.x*scalingFrame.y*scalingFrame.z;
}
