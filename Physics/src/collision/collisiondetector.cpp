#include "collisiondetector.h"
#include <cmath>
#include "glm/gtx/rotate_vector.hpp"
#include "collision/collisionresolver.h"
#include <qdebug.h>
#include "glm/ext.hpp"
#include "helpers/algebra.h"
#include "simulation/behaviour/behaviour.h"
#include "collision/interpenetrationresolver.h"
#include <iostream>
#include <limits>
#include "input/inputhandler.h"

#include "glm/ext.hpp"

#define EPSILON 0.00001

CollisionPointer CollisionDetector::collisions[3][3] = {
    //       CUBE, SPHERE, PLANE
    //CUBE
    //SPHERE
    //PLANE

    //CUBE
    {CollisionDetector::detectCubeCubeCollision,
     [](SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2)
        ->bool{ return CollisionDetector::detectSphereCubeCollision(simulationEntity2, shape2,simulationEntity1,shape1); },
     CollisionDetector::detectCubePlaneCollision
    },
    //SPHERE
    {CollisionDetector::detectSphereCubeCollision,
     CollisionDetector::detectSphereSphereCollision,
     CollisionDetector::detectSpherePlaneCollision
    },
    //PLANE
    {[](SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2)
     ->bool{ return CollisionDetector::detectCubePlaneCollision(simulationEntity2, shape2,simulationEntity1,shape1); },
     [](SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2)
        ->bool{ return CollisionDetector::detectSpherePlaneCollision(simulationEntity2, shape2,simulationEntity1,shape1); },
     nullptr
    }
};


CollisionDetector::CollisionDetector(){
}

void CollisionDetector::detectCollision(SimulationEntity *simulationEntity1, SimulationEntity *simulationEntity2){
    bool wasCollision = false;

    auto shape1It = simulationEntity1->getComplexShape()->getShapes()->begin();
    while(!wasCollision && shape1It != simulationEntity1->getComplexShape()->getShapes()->end()){
        auto shape2It = simulationEntity2->getComplexShape()->getShapes()->begin();
        while(!wasCollision && shape2It != simulationEntity2->getComplexShape()->getShapes()->end()){
            CollisionPointer collisionFunction = collisions[(int)(*shape1It)->getPrimitiveId()][(int)(*shape2It)->getPrimitiveId()];
            if (collisionFunction){
                wasCollision = (*collisionFunction)(simulationEntity1, *shape1It, simulationEntity2, *shape2It);
            }else if ((*shape1It)->getPrimitiveId() != PrimitiveId::PLANE && (*shape2It)->getPrimitiveId() != PrimitiveId::PLANE){
                qDebug() << "Not implemented collision for primitives ids : " << (int)(*shape1It)->getPrimitiveId() <<(int)(*shape2It)->getPrimitiveId();
            }
            shape2It++;
        }
        shape1It++;
    }
}

bool CollisionDetector::detectSphereSphereCollision(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2){
    mat3 shape1RotMat(simulationEntity1->getRotationMatrix());
    vec3 shape1PosInGlobalFrame = simulationEntity1->getPosition() + shape1RotMat * shape1->getOffset();
    //vec3 shape1RotInGlobalFrame = simulationEntity1->getRotation() + shape1->getRotation();

    mat3 shape2RotMat(simulationEntity2->getRotationMatrix());
    vec3 shape2PosInGlobalFrame = simulationEntity2->getPosition() + shape2RotMat * shape2->getOffset();
    //vec3 shape2RotInGlobalFrame = simulationEntity2->getRotation() + shape2->getRotation();

    bool collisionOccured = false;

    if (glm::length(shape1PosInGlobalFrame - shape2PosInGlobalFrame) <= shape1->getScalingFrame().x+shape2->getScalingFrame().x){
        if(simulationEntity1->hasBehaviours()){
            for (auto behaviour : *(simulationEntity1->getBehaviours())){
                behaviour->onCollision(simulationEntity2);
            }
        }
        if(simulationEntity2->hasBehaviours()){
            for (auto behaviour : *(simulationEntity2->getBehaviours())){
                behaviour->onCollision(simulationEntity1);
            }
        }

        if(simulationEntity1->isCollidable() && simulationEntity2->isCollidable()){
            simulationEntity1->getPhysicsComponent()->setCollision();
            simulationEntity2->getPhysicsComponent()->setCollision();
            vec3 collisionNormalInGlobalFrame = glm::normalize(shape1PosInGlobalFrame - shape2PosInGlobalFrame);

            vec3 leverArm1InGlobalFrame = shape1RotMat*shape1->getOffset() - collisionNormalInGlobalFrame;
            vec3 collisionPoint1TangentialVelInGlobalFrame = glm::cross(simulationEntity1->getPhysicsComponent()->getAngularVelocity(), leverArm1InGlobalFrame);

            vec3 leverArm2InGlobalFrame = shape2RotMat*shape2->getOffset() + collisionNormalInGlobalFrame;
            vec3 collisionPoint2TangentialVelInGlobalFrame = glm::cross(simulationEntity2->getPhysicsComponent()->getAngularVelocity(), leverArm2InGlobalFrame);

            vec3 relativeVelocityInGlobalFrame = (simulationEntity1->getPhysicsComponent()->getLinearVelocity() + collisionPoint1TangentialVelInGlobalFrame)
                                     - (simulationEntity2->getPhysicsComponent()->getLinearVelocity() + collisionPoint2TangentialVelInGlobalFrame);

            if(glm::dot(collisionNormalInGlobalFrame, glm::normalize(relativeVelocityInGlobalFrame)) < 0){
                CollisionResolver::resolveMovableMovable(collisionNormalInGlobalFrame, relativeVelocityInGlobalFrame,
                                                       simulationEntity1, leverArm1InGlobalFrame,
                                                       simulationEntity2, leverArm2InGlobalFrame);



                collisionOccured = true;
            }
            InterpenetrationResolver::linearResolution(collisionNormalInGlobalFrame,
                        shape1->getScalingFrame().x+shape2->getScalingFrame().x - glm::length(shape1PosInGlobalFrame - shape2PosInGlobalFrame),
                        simulationEntity1, simulationEntity2);

        }
    }

    return collisionOccured;
}

bool CollisionDetector::detectSpherePlaneCollision(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2){
    mat3 shape1RotMat(simulationEntity1->getRotationMatrix());
    vec3 shape1PosInGlobalFrame = simulationEntity1->getPosition() + shape1RotMat * shape1->getOffset();
    //vec3 shape1RotInGlobalFrame = simulationEntity1->getRotation() + shape1->getRotation();

    mat3 shape2RotMat(simulationEntity2->getRotationMatrix());
    vec3 shape2PosInGlobalFrame = simulationEntity2->getPosition() + shape2RotMat * shape2->getOffset();
    vec3 shape2RotInGlobalFrame = simulationEntity2->getRotation() + shape2->getRotation();

    bool collisionOccured = false;

    vec3 planeNormal = glm::rotate(glm::rotate(glm::rotate(vec3(0,0,-1), shape2RotInGlobalFrame.x, vec3(1,0,0)), shape2RotInGlobalFrame.y, vec3(0,1,0)), shape2RotInGlobalFrame.z, vec3(0,0,1));
    float planeDistToOrigin = -glm::dot(shape2PosInGlobalFrame, planeNormal);
    float dist = fabs(glm::dot(shape1PosInGlobalFrame, planeNormal) + planeDistToOrigin);
    if (dist <= shape1->getScalingFrame().x){
        if(simulationEntity1->isCollidable() && simulationEntity2->isCollidable()){
            simulationEntity1->getPhysicsComponent()->setCollision();
            vec3 leverArm1InGlobalFrame = (shape1RotMat*shape1->getOffset() - shape1->getScalingFrame().x * planeNormal);
            vec3 collisionPoint1TangentialVelInGlobalFrame = glm::cross(
                        simulationEntity1->getPhysicsComponent()->getAngularVelocity(), leverArm1InGlobalFrame);

            vec3 relativeVelocityInGlobalFrame = (simulationEntity1->getPhysicsComponent()->getLinearVelocity()
                                                  + collisionPoint1TangentialVelInGlobalFrame);

            if(glm::dot(planeNormal, glm::normalize(relativeVelocityInGlobalFrame)) < 0){
                CollisionResolver::resolveMovableStationary(planeNormal, relativeVelocityInGlobalFrame, simulationEntity1, leverArm1InGlobalFrame, simulationEntity2);
                collisionOccured = true;
            }
            InterpenetrationResolver::linearResolution(planeNormal, shape1->getScalingFrame().x - dist, simulationEntity1, simulationEntity2);
        }
    }

    return collisionOccured;
}

/**
 * @brief projectShape onto axis defined by faceNormal
 * @param shapeVertices
 * @param faceNormal
 * @return list with 2 elements - min and max projection onto plane
 */
vec2 projectShape(vector<vec3> shapeVertices, vec3 faceNormal){
    // min and max, initializing with dot of first vertex
    vec2 result(glm::dot(faceNormal, shapeVertices[0]), glm::dot(faceNormal, shapeVertices[0]));

    for(int i=1; i < shapeVertices.size(); i++){
        float length = glm::dot(faceNormal, shapeVertices[i]);
        if (length < result[0])
            result[0] = length;
        else if (length > result[1])
            result[1] = length;
    }

    return result;
}

bool CollisionDetector::detectSphereCubeCollision(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2){
    mat3 shape1RotMat(simulationEntity1->getRotationMatrix());
    vec3 shape1PosInGlobalFrame = simulationEntity1->getPosition() + shape1RotMat * shape1->getOffset();
    //vec3 shape1RotInGlobalFrame = simulationEntity1->getRotation() + shape1->getRotation();

    mat3 shape2RotMat(simulationEntity2->getRotationMatrix());
    vec3 shape2PosInGlobalFrame = simulationEntity2->getPosition() + shape2RotMat * shape2->getOffset();
    //vec3 shape2RotInGlobalFrame = simulationEntity2->getRotation() + shape2->getRotation();

    bool collisionOccured = false;
    bool deepCollision = false;

    // calculate closest point from centre of sphere to cuboid
    vec3 d = shape1PosInGlobalFrame - shape2PosInGlobalFrame;
    vec3 closestPoint = shape2PosInGlobalFrame;
    vec3 u[3] = { vec3(1.0f, 0, 0), vec3(0, 1.0f, 0), vec3(0, 0, 1.0f) };

    mat3 rotationMatrix = shape2RotMat * mat3(Algebra::createRotationMatrix(shape2->getRotation()));

    vec3 cubeScale = shape2->getScalingFrame();
    float sphereScale = shape1->getScalingFrame().x;

    for(int i=0; i<3; i++){
        u[i] = rotationMatrix * u[i];

        float dist = glm::dot(d, u[i]);
        if(dist > cubeScale[i]){
            dist = cubeScale[i];
        }else if(dist < -cubeScale[i]){
            dist = -cubeScale[i];
        }
        closestPoint += dist * u[i];
    }


    vec3 referenceNormal;
    float leastPenetration = std::numeric_limits<float>::max();
    // sphere is inside cube, we need to perform SAT to get proper contact data
    //http://media.steampowered.com/apps/valve/2015/DirkGregorius_Contacts.pdf slide 57
    if (glm::length(closestPoint - shape1PosInGlobalFrame) <= EPSILON){
        deepCollision = true;
        vector<vec3> cubeVertices = getGlobalFrameVertices(shape2PosInGlobalFrame, shape2->getScalingFrame(), u[0], u[1], u[2]);

        for(int i=0; i<3; i++){
            vec2 cubeProjection = projectShape(cubeVertices, glm::abs(u[i]));
            vec2 sphereProjection = projectShape({shape1PosInGlobalFrame - sphereScale * u[i], shape1PosInGlobalFrame + sphereScale * u[i]}, glm::abs(u[i]));

            // if doesn't overlap at least at one axis according to SAT there's no collision
            // its basically min1 - max2 > 0 or min2 - max1 > 0
            float penetration1 = fabs(cubeProjection[0] - sphereProjection[1]);
            float penetration2 = fabs(sphereProjection[0] - cubeProjection[1]);
            float penetration;
            if (penetration1 < penetration2){
                penetration = penetration1;
                u[i] *= -1;
            }else{
                penetration = penetration2;
            }

            if (penetration < leastPenetration){
                leastPenetration = penetration;
                referenceNormal = u[i];
            }
        }

        closestPoint = shape1PosInGlobalFrame + (leastPenetration - sphereScale) * referenceNormal;
    }

    // check if it intersects
    vec3 v = closestPoint - shape1PosInGlobalFrame;

    if(glm::dot(v,v) <= sphereScale * sphereScale || deepCollision){
        if(simulationEntity1->hasBehaviours()){
            for (auto behaviour : *(simulationEntity1->getBehaviours())){
                behaviour->onCollision(simulationEntity2);
            }
        }
        if(simulationEntity2->hasBehaviours()){
            for (auto behaviour : *(simulationEntity2->getBehaviours())){
                behaviour->onCollision(simulationEntity1);
            }
        }
        if(simulationEntity1->isCollidable() && simulationEntity2->isCollidable()){
            simulationEntity1->getPhysicsComponent()->setCollision();
            simulationEntity2->getPhysicsComponent()->setCollision();

            // according to http://media.steampowered.com/apps/valve/2015/DirkGregorius_Contacts.pdf slide 55
            vec3 collisionNormalInGlobalFrame = deepCollision ?
                        referenceNormal : glm::normalize(shape1PosInGlobalFrame - closestPoint);

            vec3 leverArm1InGlobalFrame = shape1RotMat*shape1->getOffset() - sphereScale*collisionNormalInGlobalFrame;
            vec3 collisionPoint1TangentialVelInGlobalFrame = glm::cross(simulationEntity1->getPhysicsComponent()->getAngularVelocity(), leverArm1InGlobalFrame);

            vec3 leverArm2InGlobalFrame = closestPoint - simulationEntity2->getPosition();
            vec3 collisionPoint2TangentialVelInGlobalFrame = glm::cross(simulationEntity2->getPhysicsComponent()->getAngularVelocity(), leverArm2InGlobalFrame);

            vec3 relativeVelocityInGlobalFrame = (simulationEntity1->getPhysicsComponent()->getLinearVelocity() + collisionPoint1TangentialVelInGlobalFrame)
                                     - (simulationEntity2->getPhysicsComponent()->getLinearVelocity() + collisionPoint2TangentialVelInGlobalFrame);

            if(glm::dot(collisionNormalInGlobalFrame, glm::normalize(relativeVelocityInGlobalFrame)) < 0.0f){
                CollisionResolver::resolveMovableMovable(collisionNormalInGlobalFrame, relativeVelocityInGlobalFrame,
                                                       simulationEntity1, leverArm1InGlobalFrame,
                                                       simulationEntity2, leverArm2InGlobalFrame);


                collisionOccured = true;
            }
            float interpenetrationDepth = deepCollision ?
                       leastPenetration : fabs(sphereScale - fabs(glm::distance(shape1PosInGlobalFrame, closestPoint)));
            InterpenetrationResolver::linearResolution(collisionNormalInGlobalFrame, interpenetrationDepth, simulationEntity1, simulationEntity2);
        }
    }

    return collisionOccured;
}

vector<vec3> CollisionDetector::getGlobalFrameVertices(vec3 centre, vec3 scale, vec3 xBase, vec3 yBase, vec3 zBase){
    vector<vec3> result = {
        centre + scale.x * xBase + scale.y * yBase + scale.z * zBase,
        centre + scale.x * xBase + scale.y * yBase - scale.z * zBase,
        centre + scale.x * xBase - scale.y * yBase + scale.z * zBase,
        centre - scale.x * xBase + scale.y * yBase + scale.z * zBase,
        centre + scale.x * xBase - scale.y * yBase - scale.z * zBase,
        centre - scale.x * xBase + scale.y * yBase - scale.z * zBase,
        centre - scale.x * xBase - scale.y * yBase + scale.z * zBase,
        centre - scale.x * xBase - scale.y * yBase - scale.z * zBase
    };

    return result;
}

bool CollisionDetector::detectCubePlaneCollision(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2){
    mat3 shape1RotMat(simulationEntity1->getRotationMatrix());
    vec3 shape1PosInGlobalFrame = simulationEntity1->getPosition() + shape1RotMat * shape1->getOffset();
    //vec3 shape1RotInGlobalFrame = simulationEntity1->getRotation() + shape1->getRotation();

    mat3 shape2RotMat(simulationEntity2->getRotationMatrix());
    vec3 shape2PosInGlobalFrame = simulationEntity2->getPosition() + shape2RotMat * shape2->getOffset();
    vec3 shape2RotInGlobalFrame = simulationEntity2->getRotation() + shape2->getRotation();

    vec3 planeNormal = glm::rotate(glm::rotate(glm::rotate(vec3(0,0,-1), shape2RotInGlobalFrame.x, vec3(1,0,0)), shape2RotInGlobalFrame.y, vec3(0,1,0)), shape2RotInGlobalFrame.z, vec3(0,0,1));
    float planeDistToOrigin = -glm::dot(shape2PosInGlobalFrame, planeNormal);
    float s = fabs(glm::dot(planeNormal, shape1PosInGlobalFrame) + planeDistToOrigin);

    vec3 cubeScale = shape1->getScalingFrame();
    vec3 u[3] = { vec3(1.0f, 0, 0), vec3(0, 1.0f, 0), vec3(0, 0, 1.0f) };
    mat3 rotationMatrix = shape1RotMat * mat3(Algebra::createRotationMatrix(shape1->getRotation()));

    for(int i=0; i<3; i++){
        u[i] = rotationMatrix * u[i];
    }

    float r = cubeScale.x * fabs(glm::dot(planeNormal, u[0])) +
              cubeScale.y * fabs(glm::dot(planeNormal, u[1])) +
              cubeScale.z * fabs(glm::dot(planeNormal, u[2]));

    if ( s <= r ) {
        if(simulationEntity1->hasBehaviours()){
            for (auto behaviour : *(simulationEntity1->getBehaviours())){
                behaviour->onCollision(simulationEntity2);
            }
        }
        if(simulationEntity1->isCollidable() && simulationEntity2->isCollidable()){
            simulationEntity1->getPhysicsComponent()->setCollision();
            vector<vec3> shape1Vertices = getGlobalFrameVertices(shape1PosInGlobalFrame, cubeScale, u[0], u[1], u[2]);
            vector<vec3> collisionPoints;
            float dot=glm::dot(shape1Vertices[0] - shape1PosInGlobalFrame, -planeNormal);
            for (auto i : shape1Vertices){
                float current_dot = glm::dot(i - shape1PosInGlobalFrame, -planeNormal);
                if(current_dot - dot >= -EPSILON){
                    if(current_dot - dot > EPSILON){
                        collisionPoints.clear();
                    }
                    collisionPoints.push_back(i);
                    dot = std::max(current_dot, dot);
                }
            }
            vec3 collisionPoint;
            for(vec3 i : collisionPoints){
                collisionPoint += i;
            }
            collisionPoint /= (float)collisionPoints.size();
            vec3 leverArm1InGlobalFrame = collisionPoint - shape1PosInGlobalFrame;
            vec3 collisionPoint1TangentialVelInGlobalFrame = glm::cross(simulationEntity1->getPhysicsComponent()->getAngularVelocity(), leverArm1InGlobalFrame);
            vec3 relativeVelocityInGlobalFrame = (simulationEntity1->getPhysicsComponent()->getLinearVelocity() + collisionPoint1TangentialVelInGlobalFrame);

            if(glm::dot(planeNormal, glm::normalize(relativeVelocityInGlobalFrame)) < EPSILON){
                CollisionResolver::resolveMovableStationary(planeNormal, relativeVelocityInGlobalFrame, simulationEntity1, leverArm1InGlobalFrame, simulationEntity2);
            }

            InterpenetrationResolver::linearResolution(planeNormal, r-s, simulationEntity1, simulationEntity2);
            return true;
        }
    }

    return false;
}

bool isPointInAABB(vec3 point, vec3 AABB){
    return glm::abs(point.x) < AABB.x + EPSILON &&
           glm::abs(point.y) < AABB.y + EPSILON &&
           glm::abs(point.z) < AABB.z + EPSILON;
}

void findVertToPlaneRemovingInterpenetrationDir(vec3 aabb, vec3 vertPos,
                                                vec3 normal1, vec3 normal2, vec3 normal3,
                                                vec3 *removingDir, float *removingDepth){
    vec3 auxRemoving = vec3(0,0,0);
    float auxDepth = 0;
    bool removingFound = false;
    if (normal1.x <= 0 && normal2.x <= 0 && normal3.x <= 0){
        auxRemoving = vec3(1,0,0);
        auxDepth = glm::abs(glm::dot(auxRemoving, aabb)) -
                            glm::dot(auxRemoving, vertPos);
        (*removingDir) = auxRemoving;
        (*removingDepth) = auxDepth;
        removingFound = true;
    }
    if (normal1.x >= 0 && normal2.x >= 0 && normal3.x >= 0){
        auxRemoving = vec3(-1,0,0);
        auxDepth = glm::abs(glm::dot(auxRemoving, aabb)) -
                            glm::dot(auxRemoving, vertPos);
        if (auxDepth < (*removingDepth) || !removingFound){
            (*removingDir) = auxRemoving;
            (*removingDepth) = auxDepth;
            removingFound = true;
        }
    }
    if (normal1.y <= 0 && normal2.y <= 0 && normal3.y <= 0){
        auxRemoving = vec3(0,1,0);
        auxDepth = glm::abs(glm::dot(auxRemoving, aabb)) -
                            glm::dot(auxRemoving, vertPos);
        if (auxDepth < (*removingDepth) || !removingFound){
            (*removingDir) = auxRemoving;
            (*removingDepth) = auxDepth;
            removingFound = true;
        }
    }
    if (normal1.y >= 0 && normal2.y >= 0 && normal3.y >= 0){
        auxRemoving = vec3(0,-1,0);
        auxDepth = glm::abs(glm::dot(auxRemoving, aabb)) -
                            glm::dot(auxRemoving, vertPos);
        if (auxDepth < (*removingDepth) || !removingFound){
            (*removingDir) = auxRemoving;
            (*removingDepth) = auxDepth;
            removingFound = true;
        }
    }
    if (normal1.z <= 0 && normal2.z <= 0 && normal3.z <= 0){
        auxRemoving = vec3(0,0,1);
        auxDepth = glm::abs(glm::dot(auxRemoving, aabb)) -
                            glm::dot(auxRemoving, vertPos);
        if (auxDepth < (*removingDepth) || !removingFound){
            (*removingDir) = auxRemoving;
            (*removingDepth) = auxDepth;
            removingFound = true;
        }
    }
    if (normal1.z >= 0 && normal2.z >= 0 && normal3.z >= 0){
        auxRemoving = vec3(0,0,-1);
        auxDepth = glm::abs(glm::dot(auxRemoving, aabb)) -
                            glm::dot(auxRemoving, vertPos);
        if (auxDepth < (*removingDepth) || !removingFound){
            (*removingDir) = auxRemoving;
            (*removingDepth) = auxDepth;
            removingFound = true;
        }
    }
}

vector<Edge> generateEdges(vec3 center, vec3 normals[3]){
    vector<Edge> result;
    vec3 point = center-normals[0]-normals[1]-normals[2];
    result.push_back(Edge(point, point+2*normals[0], -normals[1]-normals[2]));
    result.push_back(Edge(point, point+2*normals[1], -normals[0]-normals[2]));
    result.push_back(Edge(point, point+2*normals[2], -normals[0]-normals[1]));

    point = center+normals[0]-normals[1]+normals[2];
    result.push_back(Edge(point, point-2*normals[0], -normals[1]+normals[2]));
    result.push_back(Edge(point, point+2*normals[1], normals[0]+normals[2]));
    result.push_back(Edge(point, point-2*normals[2], normals[0]-normals[1]));

    point = center-normals[0]+normals[1]+normals[2];
    result.push_back(Edge(point, point+2*normals[0], normals[1]+normals[2]));
    result.push_back(Edge(point, point-2*normals[1], -normals[0]+normals[2]));
    result.push_back(Edge(point, point-2*normals[2], -normals[0]+normals[1]));

    point = center+normals[0]+normals[1]-normals[2];
    result.push_back(Edge(point, point-2*normals[0], normals[1]-normals[2]));
    result.push_back(Edge(point, point-2*normals[1], normals[0]-normals[2]));
    result.push_back(Edge(point, point+2*normals[2], normals[0]+normals[1]));

    return result;
}

bool CollisionDetector::isEdgeInAABB(Edge edge, vec3 AABB){
    vec3 edgeVec = edge.point2 - edge.point1;
    vec3 edgeMidPoint = edge.point2-0.5f*edgeVec;
    vec3 edgeRad = vec3(glm::abs(0.5f*edgeVec));

    if (edgeRad.x + AABB.x < glm::abs(edgeMidPoint.x))
        return false;
    if (edgeRad.y + AABB.y < glm::abs(edgeMidPoint.y))
        return false;
    if (edgeRad.z + AABB.z < glm::abs(edgeMidPoint.z))
        return false;

    vec3 edgeDir = glm::normalize(edgeVec);

    if (fabsf(edgeDir.y * edgeMidPoint.z - edgeDir.z * edgeMidPoint.y) > fabsf(edgeDir.y) * AABB.z + fabsf(edgeDir.z) * AABB.y)
        return false;
    if (fabsf(edgeDir.z * edgeMidPoint.x - edgeDir.x * edgeMidPoint.z) > fabsf(edgeDir.z) * AABB.x + fabsf(edgeDir.x) * AABB.z)
        return false;
    if (fabsf(edgeDir.x * edgeMidPoint.y - edgeDir.y * edgeMidPoint.x) > fabsf(edgeDir.x) * AABB.y + fabsf(edgeDir.y) * AABB.x)
        return false;

    return true;
}

pair<vec3, vec3> CollisionDetector::getClosestPointsOfEdges(vec3 p1, vec3 dir1, vec3 p2, vec3 dir2){ //p1 is point on line1, p2 on line 2, works only for non parallel lines
    vec3 r = p1-p2;
    float s = (glm::dot(dir1, dir2) * glm::dot(dir2, r) - glm::dot(dir1, r)) / (1-glm::pow(glm::dot(dir1, dir2), 2));
    float f = (glm::dot(dir2, r) - glm::dot(dir1, dir2) * glm::dot(dir1, r)) / (1-glm::pow(glm::dot(dir1, dir2), 2));

    return pair<vec3, vec3>(p1+s*dir1, p2+f*dir2);
}

CollisionInfo CollisionDetector::checkOBBVertsAndAABB(mat3 aabbFullRotMat, vec3 aabb, mat3 gFToObbShapeLFRotMat, vec3 obbPos, vec3 obb[3], Shape *obbShape){
    CollisionInfo collisionInfo;
    for(int i=-1; i<=1; i+=2){
        for(int j=-1; j<=1; j+=2){
            for(int k=-1; k<=1; k+=2){
                vec3 vertPos = obbPos + i*obb[0]*obbShape->getScalingFrame().x
                                           + j*obb[1]*obbShape->getScalingFrame().y
                                           + k*obb[2]*obbShape->getScalingFrame().z;

                if(isPointInAABB(vertPos, aabb)){
                    vec3 interpenetrationRemovingDir = vec3(0);
                    float interpenetrationDepth = 0.f;
                    findVertToPlaneRemovingInterpenetrationDir(aabb, vertPos,
                                                               i*obb[0]*obbShape->getScalingFrame().x,
                                                               j*obb[1]*obbShape->getScalingFrame().y,
                                                               k*obb[2]*obbShape->getScalingFrame().z,
                                                               &interpenetrationRemovingDir,
                                                               &interpenetrationDepth);

                    if (glm::length(interpenetrationRemovingDir) > EPSILON){
                        vec3 collisionNormalInGF = aabbFullRotMat * interpenetrationRemovingDir;
                        vec3 s1ColPointInS1LF = interpenetrationDepth * interpenetrationRemovingDir + vertPos;
                        vec3 s2ColPointInS2LF = gFToObbShapeLFRotMat * (aabbFullRotMat * (vertPos - obbPos));
                        CollisionInfo foundCollision(s1ColPointInS1LF, s2ColPointInS2LF, collisionNormalInGF, interpenetrationDepth);

                        if (foundCollision < collisionInfo)
                            collisionInfo = foundCollision;
                    }
                }
            }
        }
    }
    return collisionInfo;
}


CollisionInfo CollisionDetector::checkOBBsVerts(mat3 s1FullRotMat, mat3 s2FullRotMat, Shape *shape1, Shape *shape2, mat3 gFToS1LFRotMat, mat3 gFToS2LFRotMat,
                                    vec3 s1PosInS2LF, vec3 s2PosInS1LF, vec3 s1NormalsInS2LF[3], glm::vec3 s2NormalsInS1LF[]){
    CollisionInfo foundCollision = checkOBBVertsAndAABB(s2FullRotMat,
                                                        shape2->getScalingFrame(),
                                                        gFToS1LFRotMat,
                                                        s1PosInS2LF,
                                                        s1NormalsInS2LF,
                                                        shape1);

    if (foundCollision.wasCollisionFound){
        vec3 aux = foundCollision.s1ColPointInS1LF;
        foundCollision.s1ColPointInS1LF = foundCollision.s2ColPointInS2LF;
        foundCollision.s2ColPointInS2LF = aux;
    }

    CollisionInfo collisionVertOfShape2WithShape1 = checkOBBVertsAndAABB(s1FullRotMat,
                                                                         shape1->getScalingFrame(),
                                                                         gFToS2LFRotMat,
                                                                         s2PosInS1LF,
                                                                         s2NormalsInS1LF,
                                                                         shape2);

    if (collisionVertOfShape2WithShape1 < foundCollision){
        collisionVertOfShape2WithShape1.collisionNormalInGF *= -1.f;
        foundCollision = collisionVertOfShape2WithShape1;
    }
    return foundCollision;
}

vector<pair<vec3, vec3>> findCounterEdgesForEdge(Edge edge, vec3 aabb){
    vector<pair<vec3, vec3>> result;

    vec3 vert1 = edge.point1.x < edge.point2.x ? edge.point1 : edge.point2;
    vec3 vert2 = edge.point1.x < edge.point2.x ? edge.point2 : edge.point1;

    vec3 obbEdgeDir = vert2 - vert1;
    vec3 aabbEdgeDir = vec3(0,0,1);
    vec3 colNormal = glm::normalize(glm::cross(obbEdgeDir, aabbEdgeDir));

    float angle = glm::acos(glm::dot(edge.normal, colNormal));

    if (vert1.y < vert2.y){
        if (angle < glm::pi<float>()/4.f)
            result.push_back(pair<vec3,vec3>(vec3(-aabb.x,aabb.y,-aabb.z), aabbEdgeDir));
        if (angle > glm::pi<float>()*3.f/4.f)
            result.push_back(pair<vec3,vec3>(vec3(aabb.x,-aabb.y,-aabb.z), aabbEdgeDir));
    } else {
        if (angle < glm::pi<float>()/4.f)
            result.push_back(pair<vec3,vec3>(vec3(aabb.x,aabb.y,-aabb.z), aabbEdgeDir));
        if (angle > glm::pi<float>()*3.f/4.f)
            result.push_back(pair<vec3,vec3>(vec3(-aabb.x,-aabb.y,-aabb.z), aabbEdgeDir));
    }

    vert1 = edge.point1.x < edge.point2.x ? edge.point1 : edge.point2;
    vert2 = edge.point1.x < edge.point2.x ? edge.point2 : edge.point1;

    obbEdgeDir = vert2 - vert1;
    aabbEdgeDir = vec3(0,1,0);
    colNormal = glm::normalize(glm::cross(obbEdgeDir, aabbEdgeDir));

    angle = glm::acos(glm::dot(edge.normal, colNormal));

    if (vert1.z < vert2.z){
        if (angle < glm::pi<float>()/4.f)
            result.push_back(pair<vec3,vec3>(vec3(aabb.x,-aabb.y,-aabb.z), aabbEdgeDir));
        if (angle > glm::pi<float>()*3.f/4.f)
            result.push_back(pair<vec3,vec3>(vec3(-aabb.x,-aabb.y,aabb.z), aabbEdgeDir));
    } else {
        if (angle < glm::pi<float>()/4.f)
            result.push_back(pair<vec3,vec3>(vec3(-aabb.x,-aabb.y,-aabb.z), aabbEdgeDir));
        if (angle > glm::pi<float>()*3.f/4.f)
            result.push_back(pair<vec3,vec3>(vec3(aabb.x,-aabb.y,aabb.z), aabbEdgeDir));
    }

    vert1 = edge.point1.z < edge.point2.z ? edge.point1 : edge.point2;
    vert2 = edge.point1.z < edge.point2.z ? edge.point2 : edge.point1;

    obbEdgeDir = vert2 - vert1;
    aabbEdgeDir = vec3(1,0,0);
    colNormal = glm::normalize(glm::cross(obbEdgeDir, aabbEdgeDir));

    angle = glm::acos(glm::dot(edge.normal, colNormal));

    if (vert1.y < vert2.y){
        if (angle < glm::pi<float>()/4.f)
            result.push_back(pair<vec3,vec3>(vec3(-aabb.x,-aabb.y,aabb.z), aabbEdgeDir));
        if (angle > glm::pi<float>()*3.f/4.f)
            result.push_back(pair<vec3,vec3>(vec3(-aabb.x,aabb.y,-aabb.z), aabbEdgeDir));
    } else {
        if (angle < glm::pi<float>()/4.f)
            result.push_back(pair<vec3,vec3>(vec3(-aabb.x,-aabb.y,-aabb.z), aabbEdgeDir));
        if (angle > glm::pi<float>()*3.f/4.f)
            result.push_back(pair<vec3,vec3>(vec3(-aabb.x,aabb.y,aabb.z), aabbEdgeDir));
    }

    return result;
}

CollisionInfo CollisionDetector::checkEdgeAndAABB(mat3 aabbFullRotMat, vec3 aabb, mat3 gFToObbShapeLFRotMat, vec3 obbPos, vec3 obb[3]){
    CollisionInfo collision;
    vector<Edge> edges = generateEdges(obbPos, obb);
    for(Edge edge : edges){
        if(isEdgeInAABB(edge, aabb)){
            vector<pair<vec3, vec3>> counterEdges = findCounterEdgesForEdge(edge, aabb);

            vec3 dir1 = glm::normalize(edge.point1 - edge.point2);
            for(auto counterEdgeInfo : counterEdges){
                CollisionInfo col1 = checkEdgeEdge(edge.point1, dir1, counterEdgeInfo.first, counterEdgeInfo.second,
                                                   aabbFullRotMat, gFToObbShapeLFRotMat, obbPos);
                if (col1 < collision)
                    collision = col1;
            }
        }
    }

    return collision;
}

CollisionInfo CollisionDetector::checkEdgeEdge(vec3 p1, vec3 dir1, vec3 p2, vec3 dir2, mat3 aabbFullRotMat, mat3 gFToObbShapeLFRotMat, vec3 obbPos){
    if(glm::dot(dir1, dir2) + EPSILON < 1.f){//check if dirs non parallel
        pair<vec3, vec3> closestPoints = getClosestPointsOfEdges(p1, dir1, p2, dir2);
        vec3 interpenetrationRemovingDir = closestPoints.second - closestPoints.first;
        float interpenetrationDepth = glm::length(interpenetrationRemovingDir);
        interpenetrationRemovingDir = glm::normalize(interpenetrationRemovingDir);

        vec3 s2ColPointInS2LF = interpenetrationDepth * interpenetrationRemovingDir + closestPoints.first;
        vec3 s1ColPointInS1LF = gFToObbShapeLFRotMat * (aabbFullRotMat * (closestPoints.first - obbPos));

        vec3 collisionNormalInGF = aabbFullRotMat * interpenetrationRemovingDir;
        CollisionInfo i(s1ColPointInS1LF, s2ColPointInS2LF, collisionNormalInGF, interpenetrationDepth);
        return i;
    }
    CollisionInfo i; //TODO: compute collision for parallel edges
    return i;
}

bool CollisionDetector::detectCubeCubeCollision(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2){
    //shape1 - s1, shape2 - s2, local frame - LF, global frame - GF
    mat3 s1RotMat(simulationEntity1->getRotationMatrix());
    vec3 s1PosInGF = simulationEntity1->getPosition() + s1RotMat * shape1->getOffset();

    mat3 s2RotMat(simulationEntity2->getRotationMatrix());
    vec3 s2PosInGF = simulationEntity2->getPosition() + s2RotMat * shape2->getOffset();

    mat3 gFToS2LFRotMat = glm::inverse(mat3(Algebra::createRotationMatrix(shape2->getRotation()))) *
                          glm::inverse(s2RotMat);
    mat3 gFToS1LFRotMat = glm::inverse(mat3(Algebra::createRotationMatrix(shape1->getRotation()))) *
                          glm::inverse(s1RotMat);

    vec3 s1PosInS2LF = gFToS2LFRotMat * (s1PosInGF - s2PosInGF);
    vec3 s2PosInS1LF = gFToS1LFRotMat * (s2PosInGF - s1PosInGF);

    mat3 s1FullRotMat = s1RotMat * mat3(Algebra::createRotationMatrix(shape1->getRotation()));
    mat3 s2FullRotMat = s2RotMat * mat3(Algebra::createRotationMatrix(shape2->getRotation()));

    vec3 s1NormalsInS2LF[] = {gFToS2LFRotMat * s1FullRotMat * vec3(1,0,0),
                              gFToS2LFRotMat * s1FullRotMat * vec3(0,1,0),
                              gFToS2LFRotMat * s1FullRotMat * vec3(0,0,1)};
    vec3 s2NormalsInS1LF[] = {gFToS1LFRotMat * s2FullRotMat * vec3(1,0,0),
                              gFToS1LFRotMat * s2FullRotMat * vec3(0,1,0),
                              gFToS1LFRotMat * s2FullRotMat * vec3(0,0,1)};

    CollisionInfo foundCollision;

    //checking vertices
    foundCollision = checkOBBsVerts(s1FullRotMat, s2FullRotMat, shape1, shape2, gFToS1LFRotMat, gFToS2LFRotMat, s1PosInS2LF, s2PosInS1LF, s1NormalsInS2LF, s2NormalsInS1LF);
    ////////////////////////
    s1NormalsInS2LF[0] *= shape1->getScalingFrame().x;
    s1NormalsInS2LF[1] *= shape1->getScalingFrame().y;
    s1NormalsInS2LF[2] *= shape1->getScalingFrame().z;
    CollisionInfo edgeCollision = checkEdgeAndAABB(s2FullRotMat, shape2->getScalingFrame(), gFToS1LFRotMat, s1PosInS2LF, s1NormalsInS2LF);
    if (edgeCollision < foundCollision){
        foundCollision = edgeCollision;
    }
    /////////////////////////
    if(foundCollision.wasCollisionFound){
        if(simulationEntity1->isCollidable() && simulationEntity2->isCollidable()){
            vec3 leverArm1InGlobalFrame = s1RotMat*shape1->getOffset() + s1FullRotMat * foundCollision.s1ColPointInS1LF;
            vec3 collisionPoint1TangentialVelInGlobalFrame = glm::cross(simulationEntity1->getPhysicsComponent()->getAngularVelocity(), leverArm1InGlobalFrame);

            vec3 leverArm2InGlobalFrame = s2RotMat*shape2->getOffset() + s2FullRotMat * foundCollision.s2ColPointInS2LF;
            vec3 collisionPoint2TangentialVelInGlobalFrame = glm::cross(simulationEntity2->getPhysicsComponent()->getAngularVelocity(), leverArm2InGlobalFrame);

            vec3 relativeVelocityInGlobalFrame = (simulationEntity1->getPhysicsComponent()->getLinearVelocity() + collisionPoint1TangentialVelInGlobalFrame)
                                     - (simulationEntity2->getPhysicsComponent()->getLinearVelocity() + collisionPoint2TangentialVelInGlobalFrame);

            bool flag = false;
            if(glm::dot(foundCollision.collisionNormalInGF, glm::normalize(relativeVelocityInGlobalFrame)) < 0){
                CollisionResolver::resolveMovableMovable(foundCollision.collisionNormalInGF, relativeVelocityInGlobalFrame,
                                                       simulationEntity1, leverArm1InGlobalFrame,
                                                       simulationEntity2, leverArm2InGlobalFrame);
                flag = true;
            }

            InterpenetrationResolver::linearResolution(foundCollision.collisionNormalInGF, foundCollision.interpenetrationDepth, simulationEntity1, simulationEntity2);
            if (flag)
                return true;
        }
    }

    return false;
}

/*
 * Old cube cube collision
 *

bool CollisionDetector::detectCubeCubeCollision2(SimulationEntity *simulationEntity1, Shape *shape1, SimulationEntity *simulationEntity2, Shape *shape2){
    mat3 shape1RotMat(simulationEntity1->getRotationMatrix());
    vec3 shape1PosInGlobalFrame = simulationEntity1->getPosition() + shape1RotMat * shape1->getOffset();
    //vec3 shape1RotInGlobalFrame = simulationEntity1->getRotation() + shape1->getRotation();

    mat3 shape2RotMat(simulationEntity2->getRotationMatrix());
    vec3 shape2PosInGlobalFrame = simulationEntity2->getPosition() + shape2RotMat * shape2->getOffset();
    //vec3 shape2RotInGlobalFrame = simulationEntity2->getRotation() + shape2->getRotation();

    mat3 rotationMatrix1 = shape1RotMat * mat3(Algebra::createRotationMatrix(shape1->getRotation()));
    mat3 rotationMatrix2 = shape2RotMat * mat3(Algebra::createRotationMatrix(shape2->getRotation()));

    // face normals 0-2 shape1, 3-5 shape2, 6-14 cross product of edges
    vec3 u[15] = { vec3(1.0f, 0, 0), vec3(0, 1.0f, 0), vec3(0, 0, 1.0f),
                   vec3(-1.0f, 0, 0), vec3(0, -1.0f, 0), vec3(0, 0, -1.0f)};

    for(int i=0; i<6; i++){
        if (i < 3)
            u[i] = rotationMatrix1 * u[i];
        else
            u[i] = rotationMatrix2 * u[i];
    }

    // cross product of face normals from both shapes
    int id=6;
    for(int i=0; i<3; i++){// shape 1
        for(int j=0; j<3; j++){// shape 2
            // consider making vector perpendicular to u[i] instead of u[3+j] when u[i] ~= [0,0,0]
            u[id++] = glm::cross(u[i], u[3+j]);
        }
    }

    // calculate position of vertices for both cubes (in global frame)
    vector<vec3> shape1Vertices = getGlobalFrameVertices(shape1PosInGlobalFrame, shape1->getScalingFrame(), u[0], u[1], u[2]);
    vector<vec3> shape2Vertices = getGlobalFrameVertices(shape2PosInGlobalFrame, shape2->getScalingFrame(), u[3], u[4], u[5]);

    float leastPenetration = -std::numeric_limits<float>::max();
    vec3 referenceNormal;

    // for every axis parallel to face normal of both shape1 and shape2
    bool collides = true;
    int caseCode = -1;
    for(int i=0; i<6; i++){
        if ( glm::length(u[i]) < EPSILON ){
            continue;
        }

        vec2 shape1Projection = projectShape(shape1Vertices, glm::abs(u[i]));
        vec2 shape2Projection = projectShape(shape2Vertices, glm::abs(u[i]));

        // if doesn't overlap at least at one axis according to SAT there's no collision
        // its basically min1 - max2 > 0 or min2 - max1 > 0 (we take highest of this number and compare with epsilon)
        float penetration1 = shape1Projection[0] - shape2Projection[1];
        float penetration2 = shape2Projection[0] - shape1Projection[1];
        float penetration;
        if (penetration1 > penetration2){
            penetration = penetration1;
            u[i] *= -1;
        }else{
            penetration = penetration2;
        }

        if (penetration > EPSILON){
            collides = false;
            break;
        }else{
            if (penetration > leastPenetration){
                leastPenetration = penetration;
                referenceNormal = u[i];
                caseCode = i;
            }
        }
    }


    if (collides) {
        int offset = (caseCode < 3 ? 3 : 0);
        float minDot = glm::dot(referenceNormal, u[offset]);
        vec3 incidentNormal = u[offset];
        for(int i=0; i<2; i++){
            float dot = glm::dot(referenceNormal, u[offset+i+1]);
            if (dot < minDot){
                minDot = dot;
                incidentNormal = u[offset+i+1];
            }
        }

        std::cout << "collision o/" << leastPenetration << glm::to_string(referenceNormal) << glm::to_string(incidentNormal) << caseCode << std::endl;
    }
    //std::cout << glm::to_string() << std::endl;

    return false;
}
*/


bool CollisionDetector::detectSphereCellCollision(vec3 spherePos, float radius, std::tuple<float, float, float> cellPos, float maxRadius){
    // calculate closest point from centre of sphere to cuboid
    vec3 cellCentre = vec3(std::get<0>(cellPos), std::get<1>(cellPos), std::get<2>(cellPos));
    vec3 d = spherePos - cellCentre;
    vec3 closestPoint = cellCentre;
    vec3 u[3] = { vec3(1.0f, 0, 0), vec3(0, 1.0f, 0), vec3(0, 0, 1.0f) };

    for(int i=0; i<3; i++){
        float dist = glm::dot(d, u[i]);
        if(dist > maxRadius){
            dist = maxRadius;
        }else if(dist < -maxRadius){
            dist = -maxRadius;
        }
        closestPoint += dist * u[i];
    }

    vec3 v = closestPoint - spherePos;
    return glm::dot(v,v) < radius * radius;
}

bool CollisionDetector::detectPlaneCellCollision(vec3 planePos, vec3 planeNormal, std::tuple<float, float, float> cellPos, float maxRadius){
    vec3 cellCentre = vec3(std::get<0>(cellPos), std::get<1>(cellPos), std::get<2>(cellPos));
    float planeDistToOrigin = -glm::dot(planePos, planeNormal);
    float s = fabs(glm::dot(planeNormal, cellCentre) + planeDistToOrigin);

    vec3 u[3] = { vec3(1.0f, 0, 0), vec3(0, 1.0f, 0), vec3(0, 0, 1.0f) };

    float r = maxRadius * (fabs(glm::dot(planeNormal, u[0])) + fabs(glm::dot(planeNormal, u[1])) + fabs(glm::dot(planeNormal, u[2])));

    return s <= r;
}
