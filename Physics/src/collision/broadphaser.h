#pragma once
#include <map>
#include <set>
#include <vector>
#include <iostream>

#include "global.h"
#include "glm/glm.hpp"

using glm::vec3;
using std::map;
using std::vector;
using std::set;
using std::pair;

class SimulationEntity;
class Scene;
class ComplexShape;

class TEST_COMMON_DLLSPEC BroadPhaser
{
public:
    BroadPhaser() {}

    void init(map<int, SimulationEntity*> entityMap, Scene* scene);
    set<set<SimulationEntity *>> broadPhase();
    int getDebugEntityId(void) {return debugEntityId;}

private:
    vector<std::tuple<float, float, float>> getCollidingCells(vec3 pos, float radius);
    vec3 getComplexAveragePosition(ComplexShape* complex);
    map<int, pair<SimulationEntity*, float>> regularEntities, hugeEntities; //id : <entity*, radius>
    map<int, pair<SimulationEntity*, vec3>> planes;  //id : <entity*, normal>
    Scene* scene;
    int debugEntityId = -1;
    float maxRadius = 0.0f;
};
