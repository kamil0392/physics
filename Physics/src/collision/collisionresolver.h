#pragma once

#include "global.h"
#include "simulation/simulationentity.h"
#include "collision/shape.h"

class TEST_COMMON_DLLSPEC CollisionResolver
{
public:
    CollisionResolver();

    static void resolveMovableMovable(vec3 collisionNormal, glm::vec3 relativeVelocity, SimulationEntity *entity1, vec3 collisionPoint1, SimulationEntity *entity2, vec3 collisionPoint2, float e=0.5f);
    static void resolveMovableStationary(glm::vec3 collisionNormal, vec3 relativeVelocity, SimulationEntity *movable, glm::vec3 collisionPointOnMovable, SimulationEntity *stationary);
};
