include(../defaults.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Physics
TEMPLATE = lib
DEFINES += TEST

SOURCES += shaders/Shader.cpp \
    shaders/ShaderCompiler.cpp \
    shaders/ShaderConfig.cpp \
    shaders/ShaderConfigProvider.cpp \
    shaders/ShaderProvider.cpp \
    helpers/BmpPicture.cpp \
    helpers/BmpReader.cpp \
    helpers/FileLoader.cpp \
    render/VertexAttribBuffer.cpp \
    render/rendercomponent.cpp \
    render/VertexBufferFactory.cpp \
    render/matrixprovider.cpp \
    input/inputhandler.cpp \
    helpers/objloader.cpp \
    collision/shape.cpp \
    collision/complexshape.cpp \
    collision/collisiondetector.cpp \
    physics/physicscomponent.cpp \
    collision/planeshape.cpp \
    simulation/camera.cpp \
    simulation/fpscounter.cpp \
    simulation/scene.cpp \
    simulation/simulation.cpp \
    simulation/simulationentity.cpp \
    simulation/timer.cpp \
    simulation/context.cpp \
    render/renderprovider.cpp \
    render/abstractrenderer.cpp \
    render/simplemvprenderer.cpp \
    collision/collisionresolver.cpp \
    render/simpleuniformnormalmvprenderer.cpp \
    collision/sphereshape.cpp \
    collision/cubeshape.cpp \
    helpers/algebra.cpp \
    simulation/behaviour/behaviour.cpp \
    simulation/behaviour/gravitysource.cpp \
    collision/interpenetrationresolver.cpp \
    simulation/entities/spring.cpp \
    simulation/entities/cable.cpp \
    simulation/entities/rod.cpp \
    collision/collisioninfo.cpp \
    collision/broadphaser.cpp \
    simulation/behaviour/follower.cpp \
    simulation/entities/rocketengine.cpp \
    simulation/behaviour/thrustsource.cpp

HEADERS  += shaders/Shader.h \
    shaders/ShaderCompiler.h \
    shaders/ShaderConfig.h \
    shaders/ShaderConfigProvider.h \
    shaders/ShaderId.h \
    shaders/ShaderProvider.h \
    helpers/BmpPicture.h \
    helpers/BmpReader.h \
    helpers/FileLoader.h \
    shaders/UniformName.h \
    render/VertexAttribBuffer.h \
    render/rendercomponent.h \
    render/VertexBufferFactory.h \
    render/matrixprovider.h \
    input/inputhandler.h \
    helpers/objloader.h \
    collision/shape.h \
    collision/complexshape.h \
    collision/collisiondetector.h \
    physics/physicscomponent.h \
    collision/planeshape.h \
    simulation/camera.h \
    simulation/fpscounter.h \
    simulation/primitiveid.h \
    simulation/scene.h \
    simulation/simulation.h \
    simulation/simulationentity.h \
    simulation/timer.h \
    simulation/context.h \
    render/renderprovider.h \
    render/abstractrenderer.h \
    render/simplemvprenderer.h \
    global.h \
    collision/collisionresolver.h \
    render/simpleuniformnormalmvprenderer.h \
    collision/sphereshape.h \
    collision/cubeshape.h \
    helpers/algebra.h \
    simulation/behaviour/behaviour.h \
    simulation/behaviour/gravitysource.h \
    collision/interpenetrationresolver.h \
    simulation/entities/spring.h \
    simulation/entities/cable.h \
    simulation/entities/rod.h \
    collision/collisioninfo.h \
    collision/broadphaser.h \
    simulation/behaviour/follower.h \
    simulation/entities/rocketengine.h \
    simulation/behaviour/thrustsource.h

LIBS += -lopengl32 -lqtmain

INCLUDEPATH += $$PWD/../libs/glm
DEPENDPATH += $$PWD/../libs/glm

#
DISTFILES += \
    simple.vert \
    simple.frag \
    simple_mvp.frag \
    simple_mvp.vert \
    simulation/behaviour/Nowy dokument tekstowy.txt

