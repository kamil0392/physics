#pragma once

#include <QMainWindow>
#include "simulation/simulation.h"

namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setSimulation(Simulation *simulation);

private:
    Ui::MainWindow *ui;
};
