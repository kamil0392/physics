include(../defaults.pri)
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

SOURCES += main.cpp \
    mainwindow.cpp \
    myopenglwidget.cpp

HEADERS += \
    mainwindow.h \
    myopenglwidget.h

DISTFILES += \
    kernel.cu

INCLUDEPATH += $$PWD/../src
DEPENDPATH += $$PWD/../src

FORMS    += mainwindow.ui

LIBS += -L../src/debug -lPhysics
#--------------------------------------
#---------CUDA-------------------------
#
MSVCRT_LINK_FLAG_DEBUG = "/MDd"
CUDA_OBJECTS_DIR = debug/cuda

OTHER_FILES +=  kernel.cu
CUDA_SOURCES += kernel.cu
CUDA_DIR = "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.0"            # Path to cuda toolkit install
SYSTEM_NAME = Win32        # Depending on your system either 'Win32', 'x64', or 'Win64'
SYSTEM_TYPE = 32            # '32' or '64', depending on your system
CUDA_ARCH = compute_10           # Type of CUDA architecture, for example 'compute_10', 'compute_11', 'sm_10'
NVCC_OPTIONS = --use_fast_math

INCLUDEPATH += $$CUDA_DIR/include/
DEPENDPATH += $$CUDA_DIR/include/
QMAKE_LIBDIR += $$CUDA_DIR/lib/$$SYSTEM_NAME
LIBS += -lcuda -lcudart

CUDA_INC = $$join(INCLUDEPATH,'" -I"','-I"','"')

## Configuration of the Cuda compiler
CONFIG(debug, debug|release) {
#    #Debug settings
#    # Debug mode
    cuda_d.input    = CUDA_SOURCES
    cuda_d.output   = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}_cuda.obj
    cuda_d.commands = $$CUDA_DIR/bin/nvcc.exe -D_DEBUG -DWIN32 $$NVCC_OPTIONS $$CUDA_INC $$LIBS \
                      --machine $$SYSTEM_TYPE \
                      --compile -cudart static -g -D_MBCS \
                      -Xcompiler "/wd4819,/EHsc,/W3,/nologo,/Od,/Zi,/RTC1" \
                      -Xcompiler $$MSVCRT_LINK_FLAG_DEBUG \
                      -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}
    cuda_d.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda_d
}

