#pragma once
#include <QOpenGLWidget>
#include <QOpenGLFunctions_1_0>
#include <QOpenGLFunctions_4_0_Core>

#include <QKeyEvent>
#include <QDebug>
#include "simulation/simulation.h"

#define MIN_OPENGL_VERSION "4.0"

class MyOpenGLWidget: public QOpenGLWidget, public QOpenGLFunctions_4_0_Core
{
public:
    MyOpenGLWidget(QWidget *parent = 0): QOpenGLWidget(parent){
        setFocusPolicy(Qt::StrongFocus);}
    void setSimulation(Simulation* simulation) { this->simulation = simulation; }

protected:
    bool init_ok;

    GLuint shaderProgram;
    GLuint VAO;
    Simulation *simulation;

    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);


    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

};
