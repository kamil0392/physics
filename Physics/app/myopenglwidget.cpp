#include "myopenglwidget.h"
#include "input/inputhandler.h"
#include <QDebug>

void MyOpenGLWidget::initializeGL(){
    // zaczynamy pesymistycznie...
    init_ok = false;

    try{

        // jaka jest dostepna wersja OpenGL?
        OpenGLVersionTest test;
        QString version = test.version();
        if (version < MIN_OPENGL_VERSION)
            throw QString("Zla wersja OpenGL: %1").arg(version);

        // specyficzne dla Qt:
        initializeOpenGLFunctions();

        // jakie mamy dostepne rozszerzenia, itp.
        //qDebug() << (char *)glGetString(GL_EXTENSIONS);
        qDebug() << (char *)glGetString(GL_RENDERER);
        qDebug() << (char *)glGetString(GL_VERSION);
        qDebug() << (char *)glGetString(GL_SHADING_LANGUAGE_VERSION);

        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);  // Enables Depth Testing
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        simulation->init();
    }
    catch (QString msg){
        qDebug() << "BLAD w initializeGL():" << msg;
    }
}


void MyOpenGLWidget::paintGL(){
    simulation->tick();

    update();
}


void MyOpenGLWidget::resizeGL(int w, int h){
    simulation->getMatrixProvider()->updateDimension(w, h);
}

void MyOpenGLWidget::keyPressEvent(QKeyEvent *event){
    InputHandler::getInstance()->setKeyState(event->key(), true);
}

void MyOpenGLWidget::keyReleaseEvent(QKeyEvent *event){
    InputHandler::getInstance()->setKeyState(event->key(), false);
}
