#version 400

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;

uniform mat4 mvp_matrix;

out vec4 fragNormal;
out vec3 fragPosition;

void main()
{
    gl_Position = position * mvp_matrix;
    fragPosition = position.xyz;
    fragNormal = normal;
}
